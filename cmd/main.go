package main

import (
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
	"gitlab.com/university/api"
	_ "gitlab.com/university/api/docs"
	"gitlab.com/university/config"
	"gitlab.com/university/pkg/logger"
	"gitlab.com/university/storage"
)

func main() {
	cfg := config.Load(".")
	logger := logger.New(cfg.LogLevel)

	var psqlUrl = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		logger.Fatal("failed to connect database: %v", err)
	}

	rdb := redis.NewClient(&redis.Options{
		Addr: cfg.Redis.Addr,
	})
	strg := storage.NewStoragePg(psqlConn)
	inMemory := storage.NewInMemoryStorage(rdb)

	server := api.New(api.Option{
		Cfg:      &cfg,
		Storage:  strg,
		InMemory: inMemory,
		Logger:   logger,
	})

	if err := server.Run(cfg.HttpPort); err != nil {
		logger.Fatal("Failed to run HTTP server:  %v", err)
		panic(err)
	}

	log.Print("Server stopped")

}
