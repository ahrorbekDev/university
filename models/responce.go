package models

type ErrorResponse struct {
	Error string `json:"error"`
}

type ResponseOK struct {
	Message string `json:"message"`
}

type StandartResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Data    any    `json:"data"`
}