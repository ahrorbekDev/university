package models

type Method string

const (
	POST   Method = "POST"
	GET    Method = "GET"
	PUT    Method = "PUT"
	PATCH  Method = "PATCH"
	DELETE Method = "DELETE"
)

type Role string

const (
	Admin Role = "admin"
	User  Role = "user"
)

// casbin object
type PermissionRequest struct {
	Role   string `json:"role"`
	Object string `json:"obj,omitempty"`
	Method string `json:"method,omitempty"`
}
