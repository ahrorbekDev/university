package models

type UserLogin struct {
	Username string `json:"username" required:"true"`
	Password string `json:"password" required:"true"`
}

type UserReq struct {
	Username        string `json:"username" required:"true"`
	Password        string `json:"password" required:"true"`
	Role            string `json:"role"`
	FullName        string `json:"full_name" required:"true"`
	BirthDate       string `json:"birth_date"`
	Avatar          string `json:"avatar"`
	BirthDistrictID uint   `json:"birth_district_id"`
}

type UserRes struct {
	ID              uint   `json:"id"`
	Username        string `json:"username" required:"true"`
	Password        string `json:"-" required:"true"`
	Role            string `json:"role" required:"true"`
	FullName        string `json:"full_name" required:"true"`
	BirthDate       string `json:"birth_date"`
	Avatar          string `json:"avatar"`
	BirthDistrictID uint   `json:"birth_district_id"`
	AccessToken     string `json:"access_token,omitempty"`
	RefreshToken    string `json:"refresh_token,omitempty"`
	CreatedAt       string `json:"created_at"`
	UpdatedAt       string `json:"updated_at,omitempty"`
	DeletedAt       string `json:"-"`
}

type GetAllRequest struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
}

type UsersList struct {
	UsersList []UserRes `json:"users_list"`
	Total     int       `json:"total"`
}

type SearchUserByName struct {
	Page  int    `json:"page"`
	Limit int    `json:"limit"`
	Name  string `json:"name"`
}

type UpdateUserRefreshToken struct {
	Id           int
	RefreshToken string
}

type UpdateUserReq struct {
	ID              int    `json:"id"`
	Username        string `json:"username" required:"true"`
	Password        string `json:"password" required:"true"`
	Role            string `json:"role" required:"true"`
	FullName        string `json:"full_name" required:"true"`
	BirthDate       string `json:"birth_date"`
	Avatar          string `json:"avatar"`
	BirthDistrictID uint   `json:"birth_district_id"`
}
