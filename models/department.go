package models

type DepartmentReq struct {
	Title    string `json:"title" required:"true"`
	Name     string `json:"name" required:"true"`
	Language string `json:"language" required:"true"`
}

type DepartmentRes struct {
	ID         int               `json:"id"`
	Title      string            `json:"title"`
	Department map[string]string `json:"department"`
}

type DepartmentUpdateData struct {
	ID       int    `json:"id"`
	Title    string `json:"title"`
	Name     string `json:"name"`
	Language string `json:"language"`
}

type DepartmentsList struct {
	TotalCount  int             `json:"total_count"`
	Departments []DepartmentRes `json:"departments"`
}
