package models

type RepublicReq struct {
	Title    string `json:"title" required:"true"`
	Name     string `json:"name" required:"true"`
	Language string `json:"language" required:"true"`
}

type RepublicRes struct {
	ID       int               `json:"id"`
	Title    string            `json:"title"`
	Republic map[string]string `json:"republic"`
}

type RepublicUpdateData struct {
	ID       int    `json:"id"`
	Title    string `json:"title"`
	Name     string `json:"name"`
	Language string `json:"language"`
}

type RepublicsList struct {
	TotalCount int           `json:"total_count"`
	Republics  []RepublicRes `json:"republics"`
}
