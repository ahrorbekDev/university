package models

type PositionReq struct {
	Title    string `json:"title" required:"true"`
	Name     string `json:"name" required:"true"`
	Language string `json:"language" required:"true"`
}

type PositionRes struct {
	ID       int               `json:"id"`
	Title    string            `json:"title"`
	Position map[string]string `json:"position"`
}

type PositionUpdateData struct {
	ID       int    `json:"id"`
	Title    string `json:"title"`
	Name     string `json:"name"`
	Language string `json:"language"`
}

type PositionsList struct {
	TotalCount int           `json:"total_count"`
	Positions  []PositionRes `json:"positions"`
}
