package models

type RegionReq struct {
	Title      string `json:"title" required:"true"`
	Name       string `json:"name" required:"true"`
	Language   string `json:"language" required:"true"`
	RepublicID uint   `json:"republic_id" required:"true"`
}

type RegionRes struct {
	ID         int               `json:"id"`
	Title      string            `json:"title"`
	RepublicID int               `json:"republic_id"`
	Region     map[string]string `json:"region"`
}

type RegionUpdateData struct {
	ID         int    `json:"id"`
	Title      string `json:"title"`
	Name       string `json:"name"`
	Language   string `json:"language"`
	RepublicID int    `json:"republic_id" required:"true"`
}

type RegionsList struct {
	TotalCount int         `json:"total_count"`
	Regions    []RegionRes `json:"regions"`
}
