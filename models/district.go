package models

type DistrictReq struct {
	Title    string `json:"title" required:"true"`
	Name     string `json:"name" required:"true"`
	Language string `json:"language" required:"true"`
	RegionID uint   `json:"region_id" required:"true"`
}

type DistrictRes struct {
	ID       int               `json:"id"`
	Title    string            `json:"title"`
	RegionID int               `json:"region_id"`
	District map[string]string `json:"district"`
}

type DistrictUpdateData struct {
	ID       int    `json:"id"`
	Title    string `json:"title"`
	Name     string `json:"name"`
	Language string `json:"language"`
	RegionID int    `json:"region_id"`
}

type DistrictsList struct {
	TotalCount int           `json:"total_count"`
	Districts  []DistrictRes `json:"districts"`
}
