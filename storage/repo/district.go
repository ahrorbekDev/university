package repo

import (
	m "gitlab.com/university/models"
)

type DistrictStorageI interface {
	CreateDistrict(req m.DistrictReq) (*m.DistrictRes, error)
	GetDistrictById(id *int) (*m.DistrictRes, error)
	GetDistrictByName(name *string) (*m.DistrictRes, error)
	GetAllDistricts(req *m.GetAllRequest) (*m.DistrictsList, error)
	SearchDistrict(req *m.SearchUserByName) (*m.DistrictsList, error)
	UpdateDistrict(req *m.DistrictUpdateData) error
	DeleteDistrict(id *int) error
}
