package repo

import (
	m "gitlab.com/university/models"
)

type RepublicStorageI interface {
	CreateRepublic(req m.RepublicReq) (*m.RepublicRes, error)
	GetRepublicById(id *int) (*m.RepublicRes, error)
	GetRepublicByName(name *string) (*m.RepublicRes, error)
	GetAllRepublics(req *m.GetAllRequest) (*m.RepublicsList, error)
	SearchRepublic(req *m.SearchUserByName) (*m.RepublicsList, error)
	UpdateRepublic(req *m.RepublicUpdateData) error
	DeleteRepublic(id *int) error
}
