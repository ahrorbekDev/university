package repo

import (
	"context"

	m "gitlab.com/university/models"
)

type AdminStorageI interface {
	Create(context.Context, *m.AdminReq) (*m.AdminRes, error)
	GetByEmail(ctx context.Context, email string) (*m.AdminRes, error)
	UpdatePassword(ctx context.Context, req *m.UpdatePassword) error
	DeleteAccount(ctx context.Context, id int) error
}
