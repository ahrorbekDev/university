package repo

import (
	m "gitlab.com/university/models"
)

type UserStorageI interface {
	CreateUser(*m.UserReq) (*m.UserRes, error)
	GetUserByUsername(username *string) (*m.UserRes, error)
	GetUserById(id *int) (*m.UserRes, error)
	GetAllUsers(req *m.GetAllRequest) (*m.UsersList, error)
	SearchUser(req *m.SearchUserByName) (*m.UsersList, error)
	UpdateUser(req *m.UpdateUserReq) error
	DeleteUser(id *int) error
}
