package repo

import (
	m "gitlab.com/university/models"
)

type PositionStorageI interface {
	CreatePosition(req m.PositionReq) (*m.PositionRes, error)
	GetPositionById(id *int) (*m.PositionRes, error)
	GetPositionByName(name *string) (*m.PositionRes, error)
	GetAllPositions(req *m.GetAllRequest) (*m.PositionsList, error)
	SearchPosition(req *m.SearchUserByName) (*m.PositionsList, error)
	UpdatePosition(req *m.PositionUpdateData) error
	DeletePosition(id *int) error
}
