package repo

import (
	m "gitlab.com/university/models"
)

type RegionStorageI interface {
	CreateRegion(req m.RegionReq) (*m.RegionRes, error)
	GetRegionById(id *int) (*m.RegionRes, error)
	GetRegionByName(name *string) (*m.RegionRes, error)
	GetAllRegions(req *m.GetAllRequest) (*m.RegionsList, error)
	SearchRegion(req *m.SearchUserByName) (*m.RegionsList, error)
	UpdateRegion(req *m.RegionUpdateData) error
	DeleteRegion(id *int) error
}
