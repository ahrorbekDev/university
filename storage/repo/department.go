package repo

import (
	m "gitlab.com/university/models"
)

type DepartmentStorageI interface {
	CreateDepartment(req m.DepartmentReq) (*m.DepartmentRes, error)
	GetDepartmentById(id *int) (*m.DepartmentRes, error)
	GetDepartmentByName(name *string) (*m.DepartmentRes, error)
	GetAllDepartments(req *m.GetAllRequest) (*m.DepartmentsList, error)
	SearchDepartment(req *m.SearchUserByName) (*m.DepartmentsList, error)
	UpdateDepartment(req *m.DepartmentUpdateData) error
	DeleteDepartment(id *int) error
}
