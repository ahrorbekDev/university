package postgres

import (
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"
	m "gitlab.com/university/models"
)

type UserRepo struct {
	db *sqlx.DB
}

func NewUserRepo(db *sqlx.DB) *UserRepo {
	return &UserRepo{
		db: db,
	}
}

func (r *UserRepo) CreateUser(req *m.UserReq) (*m.UserRes, error) {
	var (
		err       error
		res       m.UserRes
		createdAt time.Time
	)
	query := `
		INSERT INTO users(
			username,
			password,
			role,
			full_name,
			birth_date,
			avatar,
			birth_district_id
		)VALUES(
			$1, $2, $3, $4, $5, $6, $7
		)RETURNING
			id,
			username,
			role,
			full_name,
			birth_date,
			avatar,
			birth_district_id,
			created_at 
	`
	row := r.db.QueryRow(query, req.Username, req.Password, req.Role, req.FullName, req.BirthDate, req.Avatar, req.BirthDistrictID)
	err = row.Scan(&res.ID, &res.Username, &res.Role, &res.FullName, &res.BirthDate, &res.Avatar, &res.BirthDistrictID, &createdAt)
	if err != nil {
		return nil, err
	}

	res.CreatedAt = createdAt.Format(m.TimeStampLayout)

	return &res, nil
}

func (r *UserRepo) GetUserById(id *int) (*m.UserRes, error) {
	var (
		err           error
		res           m.UserRes
		nullUpdatedAt sql.NullTime
		createdAt     time.Time
	)

	query := `
		SELECT 
			id,
			username,
			role,
			full_name,
			birth_date,
			avatar,
			birth_district_id,
			created_at,
			updated_at
		FROM 
			users
		WHERE 
			id = $1
		AND
			deleted_at IS NULL
	`

	rows := r.db.QueryRow(query, id)

	err = rows.Scan(
		&res.ID,
		&res.Username,
		&res.Role,
		&res.FullName,
		&res.BirthDate,
		&res.Avatar,
		&res.BirthDistrictID,
		&createdAt,
		&nullUpdatedAt,
	)
	if err != nil {
		return nil, err
	}
	res.CreatedAt = createdAt.Format(m.TimeStampLayout)
	if nullUpdatedAt.Valid {
		res.UpdatedAt = nullUpdatedAt.Time.Format(m.TimeStampLayout)
	}

	return &res, nil
}

func (r *UserRepo) GetUserByUsername(username *string) (*m.UserRes, error) {
	var (
		err           error
		res           m.UserRes
		nullUpdatedAt sql.NullTime
		createdAt     time.Time
	)

	query := `
		SELECT 
			id,
			username,
			role,
			full_name,
			birth_date,
			avatar,
			birth_district_id,
			password,
			created_at,
			updated_at
		FROM 
			users
		WHERE 
			username = $1
		AND
			deleted_at IS NULL
	`

	rows := r.db.QueryRow(query, username)

	err = rows.Scan(
		&res.ID,
		&res.Username,
		&res.Role,
		&res.FullName,
		&res.BirthDate,
		&res.Avatar,
		&res.BirthDistrictID,
		&res.Password,
		&createdAt,
		&nullUpdatedAt,
	)
	if err != nil {
		return nil, err
	}
	res.CreatedAt = createdAt.Format(m.TimeStampLayout)
	if nullUpdatedAt.Valid {
		res.UpdatedAt = nullUpdatedAt.Time.Format(m.TimeStampLayout)
	}

	return &res, nil
}

func (r *UserRepo) GetAllUsers(req *m.GetAllRequest) (*m.UsersList, error) {
	var (
		err        error
		res        m.UsersList
		totalCount int
	)

	offset := (req.Page - 1) * req.Limit

	err = r.db.QueryRow(`SELECT COUNT(*) FROM users WHERE deleted_at IS NULL`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.Total = totalCount

	query := `
		SELECT
			id,
			username,
			role,
			full_name,
			birth_date,
			avatar,
			birth_district_id,
			created_at,
			updated_at
		FROM users
		WHERE deleted_at IS NULL
		LIMIT 
			$1 OFFSET $2`

	rows, err := r.db.Query(query, req.Limit, offset)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			nullUpdatedAt sql.NullTime
			createdAt     time.Time
			user          m.UserRes
		)
		err = rows.Scan(
			&user.ID,
			&user.Username,
			&user.Role,
			&user.FullName,
			&user.BirthDate,
			&user.Avatar,
			&user.BirthDistrictID,
			&createdAt,
			&nullUpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		if nullUpdatedAt.Valid {
			user.UpdatedAt = nullUpdatedAt.Time.Format(m.TimeStampLayout)
		}
		user.CreatedAt = createdAt.Format(m.TimeStampLayout)

		res.UsersList = append(res.UsersList, user)
	}
	return &res, nil
}

func (r *UserRepo) SearchUser(req *m.SearchUserByName) (*m.UsersList, error) {

	var (
		err        error
		res        m.UsersList
		totalCount int
		offset     int
	)

	offset = (req.Page - 1) * req.Limit

	err = r.db.QueryRow(`
		SELECT COUNT(*) FROM users 
		WHERE deleted_at IS NULL
		AND full_name ILIKE $1`, "%"+req.Name+"%").Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.Total = totalCount

	query := `
		SELECT
			id,
			username,
			role,
			full_name,
			birth_date,
			avatar,
			birth_district_id,
			created_at,
			updated_at
		FROM 
			users
		WHERE 
			deleted_at IS NULL 
		AND 
			(full_name LIKE '%' || $1 || '%' OR username LIKE '%' || $1 || '%')
		ORDER BY 
			created_at DESC, 
			id DESC 
		LIMIT $2 OFFSET $3`

	rows, err := r.db.Query(query, req.Name, req.Limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			nullUpdatedAt sql.NullTime
			createdAt     time.Time
			user          m.UserRes
		)
		err = rows.Scan(
			&user.ID,
			&user.Username,
			&user.Role,
			&user.FullName,
			&user.BirthDate,
			&user.Avatar,
			&user.BirthDistrictID,
			&createdAt,
			&nullUpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		if nullUpdatedAt.Valid {
			user.UpdatedAt = nullUpdatedAt.Time.Format(m.TimeStampLayout)
		}
		user.CreatedAt = createdAt.Format(m.TimeStampLayout)

		res.UsersList = append(res.UsersList, user)
	}

	return &res, nil
}

func (r *UserRepo) UpdateUser(req *m.UpdateUserReq) error {

	query := `
		UPDATE 
			users 
		SET
			full_name = $1,
			password = $2,
			username = $3,
			birthdate = $4,
			avatar = $5,
			birth_district_id = $6
		WHERE 
			id = $7
		AND 
			deleted_at IS NULL
	`
	_, err := r.db.Exec(query, req.FullName, req.Password, req.Username, req.BirthDate, req.Avatar, req.BirthDistrictID)
	if err != nil {
		return err
	}

	return nil
}

func (r *UserRepo) DeleteUser(id *int) error {

	query := `
		UPDATE 
			users 
		SET
			deleted_at = CURRENT_TIMESTAMP,
		WHERE 
			id = $1
	`
	_, err := r.db.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}
