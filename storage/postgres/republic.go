package postgres

import (
	"database/sql"
	"encoding/json"
	"log"

	"github.com/jmoiron/sqlx"
	m "gitlab.com/university/models"
)

type RepublicRepo struct {
	db *sqlx.DB
}

func NewRepublicRepo(db *sqlx.DB) *RepublicRepo {
	return &RepublicRepo{
		db: db,
	}
}

func (r *RepublicRepo) CreateRepublic(req m.RepublicReq) (*m.RepublicRes, error) {
	var (
		err      error
		res      m.RepublicRes
		id       sql.NullInt32
		byteData []byte
	)

	mpData := make(map[string]string)

	tz, err := r.db.Begin()
	if err != nil {
		log.Println("error while beginning transaction:", err)
		return nil, err
	}

	query := `
	SELECT 
		id
	FROM 
		republics
	WHERE 
		name = $1;
	`

	err = tz.QueryRow(query, req.Title).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			mpData[req.Language] = req.Name
			jsonData, err := json.Marshal(mpData)
			if err != nil {
				tz.Rollback()
				log.Println("errro while marshaling data : ", err)
				return nil, err
			}
			query = `
			INSERT INTO republics(
				name,
				data
			)VALUES(
				$1, $2
			)RETURNING
				id,
				name,
				data
			`
			row := tz.QueryRow(query, req.Title, jsonData)
			err = row.Scan(&res.ID, &res.Title, &byteData)
			if err != nil {
				tz.Rollback()
				return nil, err
			}

			err = json.Unmarshal(byteData, &res.Republic)
			if err != nil {
				tz.Rollback()
				log.Println("errro while umarshaling data : ", err)
				return nil, err
			}

		} else {
			tz.Rollback()
			log.Println("error while checking republic exsistance : ", err)
			return nil, err
		}

	} else if id.Valid {
		reqID := int(id.Int32)
		republic, err := r.GetRepublicById(&reqID)
		if err != nil {
			tz.Rollback()
			log.Println("error while getting republic data : ", err)
			return nil, err
		}

		republic.Republic[req.Language] = req.Name
		jsonData, err := json.Marshal(republic.Republic)
		if err != nil {
			tz.Rollback()
			log.Println("errro while marshaling data : ", err)
			return nil, err
		}

		query = `
		UPDATE republics SET data = $1
		WHERE id = $2
		RETURNING id, name, data
		`
		row := r.db.QueryRow(query, jsonData, reqID)

		err = row.Scan(&res.ID, &res.Title, &byteData)
		if err != nil {
			tz.Rollback()
			log.Println("errro while scanning rows : ", err)
			return nil, err
		}

		err = json.Unmarshal(byteData, &res.Republic)
		if err != nil {
			tz.Rollback()
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

	}

	err = tz.Commit()
	if err != nil {
		log.Println("errro while committing transaction : ", err)
		return nil, err
	}

	return &res, nil
}

func (r *RepublicRepo) GetRepublicById(id *int) (*m.RepublicRes, error) {
	var (
		err      error
		res      m.RepublicRes
		byteData []byte
	)

	query := `
		SELECT 
			id,
			name,
			data
		FROM 
			republics
		WHERE 
			id = $1
	`

	err = r.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.Title,
		&byteData,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.Republic)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (r *RepublicRepo) GetRepublicByName(name *string) (*m.RepublicRes, error) {
	var (
		err      error
		res      m.RepublicRes
		byteData []byte
	)

	query := `
	SELECT 
		id,
		name,
		data
	FROM 
		republics
	WHERE 
		name = $1
	`
	err = r.db.QueryRow(query, name).Scan(
		&res.ID,
		&res.Title,
		&byteData,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.Republic)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (r *RepublicRepo) GetAllRepublics(req *m.GetAllRequest) (*m.RepublicsList, error) {

	var (
		err        error
		res        m.RepublicsList
		totalCount int
	)

	offset := (req.Page - 1) * req.Limit

	err = r.db.QueryRow(`SELECT COUNT(*) FROM republics`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
	SELECT 
		id,
		name,
		data
	FROM 
		republics
	LIMIT 
		$1 OFFSET $2`

	rows, err := r.db.Query(query, req.Limit, offset)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			rep      m.RepublicRes
			byteData []byte
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.Republic)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Republics = append(res.Republics, rep)
	}
	return &res, nil
}

func (r *RepublicRepo) SearchRepublic(req *m.SearchUserByName) (*m.RepublicsList, error) {

	var (
		err        error
		res        m.RepublicsList
		totalCount int
		offset     int
	)

	offset = (req.Page - 1) * req.Limit

	err = r.db.QueryRow(`SELECT COUNT(*) FROM republics`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
	SELECT id, name, data FROM reoublics
    WHERE 
        EXISTS (
            SELECT 1
            FROM jsonb_each_text(data) AS obj(key, value)
            WHERE value LIKE '%' || $1 || '%'
        )
    ORDER BY id DESC
    LIMIT $2 OFFSET $3`

	rows, err := r.db.Query(query, req.Name, req.Limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			byteData []byte
			rep      m.RepublicRes
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.Republic)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Republics = append(res.Republics, rep)
	}

	return &res, nil
}

func (r *RepublicRepo) UpdateRepublic(req *m.RepublicUpdateData) error {

	republic, err := r.GetRepublicById(&req.ID)
	if err != nil {
		log.Println("error while getting republic data : ", err)
		return err
	}

	republic.Republic[req.Language] = req.Name
	jsonData, err := json.Marshal(republic.Republic)
	if err != nil {
		log.Println("errro while marshaling data : ", err)
		return err
	}

	query := `
		UPDATE 
			republics 
		SET
			data = $1
		WHERE 
			id = $2
	`
	_, err = r.db.Exec(query, jsonData, req.ID)
	if err != nil {
		return err
	}

	return nil
}

func (r *RepublicRepo) DeleteRepublic(id *int) error {

	query := `
		DELETE FROM republics WHERE id = $1
	`
	_, err := r.db.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}
