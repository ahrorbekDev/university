package postgres

import (
	"context"
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"
	m "gitlab.com/university/models"
)

type adminRepo struct {
	db *sqlx.DB
}

func NewAdminRepo(db *sqlx.DB) *adminRepo {
	return &adminRepo{
		db: db,
	}
}

func (a *adminRepo) Create(ctx context.Context, req *m.AdminReq) (*m.AdminRes, error) {
	var (
		res       m.AdminRes
		createdAt time.Time
	)

	query := `
	INSERT INTO admins(
		full_name,
		phone_number,
		email,
		password,
		role,
		refresh_token
	)VALUES($1, $2, $3, $4, $5, $6)
	RETURNING 
		id,
		full_name,
		phone_number,
		email,
		role,
		refresh_token,
		created_at
	`

	row := a.db.QueryRow(
		query,
		req.FullName,
		req.PhoneNumber,
		req.Email,
		req.Password,
		req.Role,
		req.RefreshToken,
	)

	err := row.Scan(
		&res.Id,
		&res.FullName,
		&res.PhoneNumber,
		&res.Email,
		&res.Role,
		&res.RefreshToken,
		&createdAt,
	)

	if err != nil {
		return nil, err
	}

	res.CreatedAt = createdAt.Format("2006-01-02 15:04:05")

	return &res, nil
}

func (a *adminRepo) GetByEmail(ctx context.Context, email string) (*m.AdminRes, error) {
	var admin m.AdminRes

	var (
		nullUpdatedAt sql.NullTime
		createdAt     time.Time
	)

	query := `
		SELECT
			id,
			full_name,
			phone_number,
			email,
			password,
			refresh_token,
			role,
			created_at,
			updated_at
		FROM admins
		WHERE email=$1
	`

	row := a.db.QueryRow(query, email)
	err := row.Scan(
		&admin.Id,
		&admin.FullName,
		&admin.PhoneNumber,
		&admin.Email,
		&admin.Password,
		&admin.RefreshToken,
		&admin.Role,
		&createdAt,
		&nullUpdatedAt,
	)
	if err != nil {
		return nil, err
	}
	if nullUpdatedAt.Valid {
		admin.UpdatedAt = nullUpdatedAt.Time.Format("2006-01-02 15:04:05")
	}
	admin.CreatedAt = createdAt.Format("2006-01-02 15:04:05")

	return &admin, nil
}

func (a *adminRepo) UpdatePassword(ctx context.Context, req *m.UpdatePassword) error {
	query := `UPDATE admins SET password=$1, updated_at=$2 WHERE email=$3`

	_, err := a.db.Exec(query, req.Password, time.Now(), req.Email)
	if err != nil {
		return err
	}

	return nil
}

func (a *adminRepo) DeleteAccount(ctx context.Context, id int) error {
	query := `DELETE FROM admins WHERE id=$1`

	_, err := a.db.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}
