package postgres

import (
	"database/sql"
	"encoding/json"
	"log"

	"github.com/jmoiron/sqlx"
	m "gitlab.com/university/models"
)

type DistrictRepo struct {
	db *sqlx.DB
}

func NewDistrictRepo(db *sqlx.DB) *DistrictRepo {
	return &DistrictRepo{
		db: db,
	}
}

func (d *DistrictRepo) CreateDistrict(req m.DistrictReq) (*m.DistrictRes, error) {
	var (
		err      error
		res      m.DistrictRes
		id       sql.NullInt32
		byteData []byte
	)
	mpData := make(map[string]string)

	tz, err := d.db.Begin()
	if err != nil {
		log.Println("error while beginning transaction:", err)
		return nil, err
	}

	query := `
	SELECT 
		id
	FROM 
		districts
	WHERE 
		name = $1
	`

	err = tz.QueryRow(query, req.Title).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			mpData[req.Language] = req.Name
			jsonData, err := json.Marshal(mpData)
			if err != nil {
				tz.Rollback()
				log.Println("errro while marshaling data : ", err)
				return nil, err
			}
			query = `
			INSERT INTO districts(
				region_id,
				name,
				data
			)VALUES(
				$1,
				$2,
				$3
			)RETURNING
				id,
				region_id,
				name,
				data
			`
			row := tz.QueryRow(query, req.RegionID, req.Title, jsonData)
			err = row.Scan(&res.ID, &res.RegionID, &res.Title, &byteData)
			if err != nil {
				tz.Rollback()
				return nil, err
			}

			err = json.Unmarshal(byteData, &res.District)
			if err != nil {
				tz.Rollback()
				log.Println("errro while umarshaling data : ", err)
				return nil, err
			}

		} else {
			tz.Rollback()
			log.Println("error while checking District exsistance : ", err)
			return nil, err
		}

	} else if id.Valid {
		reqID := int(id.Int32)
		District, err := d.GetDistrictById(&reqID)
		if err != nil {
			tz.Rollback()
			log.Println("error while getting district data : ", err)
			return nil, err
		}

		District.District[req.Language] = req.Name
		jsonData, err := json.Marshal(District.District)
		if err != nil {
			tz.Rollback()
			log.Println("errro while marshaling data : ", err)
			return nil, err
		}

		query = `
		UPDATE districts SET data = $1, region_id = $2
		WHERE id = $3
		RETURNING id, region_id, name, data;
		`
		row := d.db.QueryRow(query, jsonData, req.RegionID, reqID)

		err = row.Scan(&res.ID, &res.RegionID, &res.Title, &byteData)
		if err != nil {
			tz.Rollback()
			log.Println("errro while scanning rows : ", err)
			return nil, err
		}

		err = json.Unmarshal(byteData, &res.District)
		if err != nil {
			tz.Rollback()
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

	}

	err = tz.Commit()
	if err != nil {
		log.Println("errro while committing transaction : ", err)
		return nil, err
	}

	return &res, nil
}

func (d *DistrictRepo) GetDistrictById(id *int) (*m.DistrictRes, error) {
	var (
		err      error
		res      m.DistrictRes
		byteData []byte
	)

	query := `
		SELECT 
			id,
			name,
			region_id,
			data
		FROM 
			districts
		WHERE 
			id = $1
	`

	err = d.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.Title,
		&res.RegionID,
		&byteData,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.District)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (d *DistrictRepo) GetDistrictByName(name *string) (*m.DistrictRes, error) {
	var (
		err      error
		res      m.DistrictRes
		byteData []byte
	)

	query := `
	SELECT 
		id,
		name,
		data,
		region_id
	FROM 
		districts
	WHERE 
		name = $1
	`
	err = d.db.QueryRow(query, name).Scan(
		&res.ID,
		&res.Title,
		&byteData,
		&res.RegionID,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.District)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (d *DistrictRepo) GetAllDistricts(req *m.GetAllRequest) (*m.DistrictsList, error) {
	var (
		err        error
		res        m.DistrictsList
		totalCount int
	)

	offset := (req.Page - 1) * req.Limit

	err = d.db.QueryRow(`SELECT COUNT(*) FROM districts`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
	SELECT 
		id,
		name,
		data,
		region_id
	FROM 
		districts
	LIMIT 
		$1 OFFSET $2`

	rows, err := d.db.Query(query, req.Limit, offset)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			rep      m.DistrictRes
			byteData []byte
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
			&rep.RegionID,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.District)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Districts = append(res.Districts, rep)
	}
	return &res, nil
}

func (d *DistrictRepo) SearchDistrict(req *m.SearchUserByName) (*m.DistrictsList, error) {

	var (
		err        error
		res        m.DistrictsList
		totalCount int
		offset     int
	)

	offset = (req.Page - 1) * req.Limit

	err = d.db.QueryRow(`SELECT COUNT(*) FROM districts`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
	SELECT id, name, data, region_id FROM districts
    WHERE 
        EXISTS (
            SELECT 1
            FROM jsonb_each_text(data) AS obj(key, value)
            WHERE value LIKE '%' || $1 || '%'
        )
    ORDER BY id DESC
    LIMIT $2 OFFSET $3`

	rows, err := d.db.Query(query, req.Name, req.Limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			byteData []byte
			rep      m.DistrictRes
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
			&rep.RegionID,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.District)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Districts = append(res.Districts, rep)
	}

	return &res, nil
}

func (d *DistrictRepo) UpdateDistrict(req *m.DistrictUpdateData) error {

	District, err := d.GetDistrictById(&req.ID)
	if err != nil {
		log.Println("error while getting District data : ", err)
		return err
	}

	District.District[req.Language] = req.Name
	jsonData, err := json.Marshal(District.District)
	if err != nil {
		log.Println("errro while marshaling data : ", err)
		return err
	}

	query := `
		UPDATE 
			districts 
		SET
			name = $1,
			data = $2,
			distric_id = $3
		WHERE 
			id = $4
	`
	_, err = d.db.Exec(query, req.Title, jsonData, req.RegionID, req.ID)
	if err != nil {
		return err
	}

	return nil
}

func (d *DistrictRepo) DeleteDistrict(id *int) error {

	query := `
		DELETE FROM districts WHERE id = $1
	`
	_, err := d.db.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}
