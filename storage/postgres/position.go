package postgres

import (
	"database/sql"
	"encoding/json"
	"log"

	"github.com/jmoiron/sqlx"
	m "gitlab.com/university/models"
)

type PositionRepo struct {
	db *sqlx.DB
}

func NewPositionRepo(db *sqlx.DB) *PositionRepo {
	return &PositionRepo{
		db: db,
	}
}

func (p *PositionRepo) CreatePosition(req m.PositionReq) (*m.PositionRes, error) {
	var (
		err      error
		res      m.PositionRes
		id       sql.NullInt32
		byteData []byte
	)

	mpData := make(map[string]string)

	tz, err := p.db.Begin()
	if err != nil {
		log.Println("error while beginning transaction:", err)
		return nil, err
	}

	query := `
	SELECT 
		id
	FROM 
		positions
	WHERE 
		name = $1
	`

	err = tz.QueryRow(query, req.Title).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			mpData[req.Language] = req.Name
			jsonData, err := json.Marshal(mpData)
			if err != nil {
				tz.Rollback()
				log.Println("errro while marshaling data : ", err)
				return nil, err
			}
			query = `
			INSERT INTO positions(
				name,
				data
			)VALUES(
				$1, $2
			)RETURNING
				id,
				name,
				data
			`
			row := tz.QueryRow(query, req.Title, jsonData)
			err = row.Scan(&res.ID, &res.Title, &byteData)
			if err != nil {
				tz.Rollback()
				return nil, err
			}

			err = json.Unmarshal(byteData, &res.Position)
			if err != nil {
				tz.Rollback()
				log.Println("errro while umarshaling data : ", err)
				return nil, err
			}

		} else {
			tz.Rollback()
			log.Println("error while checking Position exsistance : ", err)
			return nil, err
		}

	} else if id.Valid {
		reqID := int(id.Int32)
		Position, err := p.GetPositionById(&reqID)
		if err != nil {
			tz.Rollback()
			log.Println("error while getting Position data : ", err)
			return nil, err
		}

		Position.Position[req.Language] = req.Name
		jsonData, err := json.Marshal(Position.Position)
		if err != nil {
			tz.Rollback()
			log.Println("errro while marshaling data : ", err)
			return nil, err
		}

		query = `
		UPDATE positions SET data = $1
		WHERE id = $2
		RETURNING id, name, data
		`
		row := p.db.QueryRow(query, jsonData, reqID)

		err = row.Scan(&res.ID, &res.Title, &byteData)
		if err != nil {
			tz.Rollback()
			log.Println("errro while scanning rows : ", err)
			return nil, err
		}

		err = json.Unmarshal(byteData, &res.Position)
		if err != nil {
			tz.Rollback()
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

	}

	err = tz.Commit()
	if err != nil {
		log.Println("errro while committing transaction : ", err)
		return nil, err
	}

	return &res, nil
}

func (p *PositionRepo) GetPositionById(id *int) (*m.PositionRes, error) {
	var (
		err      error
		res      m.PositionRes
		byteData []byte
	)

	query := `
		SELECT 
			id,
			name,
			data
		FROM 
			positions
		WHERE 
			id = $1
	`

	err = p.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.Title,
		&byteData,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.Position)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (p *PositionRepo) GetPositionByName(name *string) (*m.PositionRes, error) {
	var (
		err      error
		res      m.PositionRes
		byteData []byte
	)

	query := `
	SELECT 
		id,
		name,
		data
	FROM 
		positions
	WHERE 
		name = $1
	`
	err = p.db.QueryRow(query, name).Scan(
		&res.ID,
		&res.Title,
		&byteData,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.Position)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (p *PositionRepo) GetAllPositions(req *m.GetAllRequest) (*m.PositionsList, error) {

	var (
		err        error
		res        m.PositionsList
		totalCount int
	)

	offset := (req.Page - 1) * req.Limit

	err = p.db.QueryRow(`SELECT COUNT(*) FROM positions`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
	SELECT 
		id,
		name,
		data
	FROM 
		positions
	LIMIT 
		$1 OFFSET $2`

	rows, err := p.db.Query(query, req.Limit, offset)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			rep      m.PositionRes
			byteData []byte
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.Position)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Positions = append(res.Positions, rep)
	}
	return &res, nil
}

func (p *PositionRepo) SearchPosition(req *m.SearchUserByName) (*m.PositionsList, error) {

	var (
		err        error
		res        m.PositionsList
		totalCount int
		offset     int
	)

	offset = (req.Page - 1) * req.Limit

	err = p.db.QueryRow(`SELECT COUNT(*) FROM positions`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
    SELECT id, name, data FROM positions
    WHERE 
        EXISTS (
            SELECT 1
            FROM jsonb_each_text(data) AS obj(key, value)
            WHERE value LIKE '%' || $1 || '%'
        )
    ORDER BY id DESC
    LIMIT $2 OFFSET $3`

	rows, err := p.db.Query(query, req.Name, req.Limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			byteData []byte
			rep      m.PositionRes
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.Position)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Positions = append(res.Positions, rep)
	}

	return &res, nil
}

func (p *PositionRepo) UpdatePosition(req *m.PositionUpdateData) error {

	Position, err := p.GetPositionById(&req.ID)
	if err != nil {
		log.Println("error while getting position data : ", err)
		return err
	}

	Position.Position[req.Language] = req.Name
	jsonData, err := json.Marshal(Position.Position)
	if err != nil {
		log.Println("errro while marshaling data : ", err)
		return err
	}

	query := `
		UPDATE 
			positions 
		SET
			data = $1
		WHERE 
			id = $2
	`
	_, err = p.db.Exec(query, jsonData, req.ID)
	if err != nil {
		return err
	}

	return nil
}

func (p *PositionRepo) DeletePosition(id *int) error {

	query := `
		DELETE FROM positions WHERE id = $1
	`
	_, err := p.db.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}
