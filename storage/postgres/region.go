package postgres

import (
	"database/sql"
	"encoding/json"
	"log"

	"github.com/jmoiron/sqlx"
	m "gitlab.com/university/models"
)

type RegionRepo struct {
	db *sqlx.DB
}

func NewRegionRepo(db *sqlx.DB) *RegionRepo {
	return &RegionRepo{
		db: db,
	}
}

func (r *RegionRepo) CreateRegion(req m.RegionReq) (*m.RegionRes, error) {
	var (
		err      error
		res      m.RegionRes
		id       sql.NullInt32
		byteData []byte
	)
	mpData := make(map[string]string)

	tz, err := r.db.Begin()
	if err != nil {
		log.Println("error while beginning transaction:", err)
		return nil, err
	}

	query := `
	SELECT 
		id
	FROM 
		regions
	WHERE 
		name = $1
	`

	err = tz.QueryRow(query, req.Title).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			mpData[req.Language] = req.Name
			jsonData, err := json.Marshal(mpData)
			if err != nil {
				tz.Rollback()
				log.Println("errro while marshaling data : ", err)
				return nil, err
			}
			query = `
			INSERT INTO regions(
				republic_id,
				name,
				data
			)VALUES(
				$1,
				$2,
				$3
			)RETURNING
				id,
				republic_id,
				name,
				data
			`
			row := tz.QueryRow(query, req.RepublicID, req.Title, jsonData)
			err = row.Scan(&res.ID, &res.RepublicID, &res.Title, &byteData)
			if err != nil {
				tz.Rollback()
				return nil, err
			}

			err = json.Unmarshal(byteData, &res.Region)
			if err != nil {
				tz.Rollback()
				log.Println("errro while umarshaling data : ", err)
				return nil, err
			}

		} else {
			tz.Rollback()
			log.Println("error while checking Region exsistance : ", err)
			return nil, err
		}

	} else if id.Valid {
		reqID := int(id.Int32)
		Region, err := r.GetRegionById(&reqID)
		if err != nil {
			tz.Rollback()
			log.Println("error while getting Region data : ", err)
			return nil, err
		}

		Region.Region[req.Language] = req.Name
		jsonData, err := json.Marshal(Region.Region)
		if err != nil {
			tz.Rollback()
			log.Println("errro while marshaling data : ", err)
			return nil, err
		}

		query = `
		UPDATE regions SET data = $1, republic_id = $2
		WHERE id = $3
		RETURNING id, republic_id, name, data;
		`
		row := r.db.QueryRow(query, jsonData, req.RepublicID, reqID)

		err = row.Scan(&res.ID, &res.RepublicID, &res.Title, &byteData)
		if err != nil {
			tz.Rollback()
			log.Println("errro while scanning rows : ", err)
			return nil, err
		}

		err = json.Unmarshal(byteData, &res.Region)
		if err != nil {
			tz.Rollback()
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

	}

	err = tz.Commit()
	if err != nil {
		log.Println("errro while committing transaction : ", err)
		return nil, err
	}

	return &res, nil
}

func (r *RegionRepo) GetRegionById(id *int) (*m.RegionRes, error) {
	var (
		err      error
		res      m.RegionRes
		byteData []byte
	)

	query := `
		SELECT 
			id,
			name,
			republic_id,
			data
		FROM 
			regions
		WHERE 
			id = $1
	`

	err = r.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.Title,
		&res.RepublicID,
		&byteData,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.Region)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (r *RegionRepo) GetRegionByName(name *string) (*m.RegionRes, error) {
	var (
		err      error
		res      m.RegionRes
		byteData []byte
	)

	query := `
	SELECT 
		id,
		name,
		data,
		republic_id
	FROM 
		regions
	WHERE 
		name = $1
	`
	err = r.db.QueryRow(query, name).Scan(
		&res.ID,
		&res.Title,
		&byteData,
		&res.RepublicID,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.Region)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (r *RegionRepo) GetAllRegions(req *m.GetAllRequest) (*m.RegionsList, error) {
	var (
		err        error
		res        m.RegionsList
		totalCount int
	)

	offset := (req.Page - 1) * req.Limit

	err = r.db.QueryRow(`SELECT COUNT(*) FROM regions`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
	SELECT 
		id,
		name,
		data,
		republic_id
	FROM 
		regions
	LIMIT 
		$1 OFFSET $2`

	rows, err := r.db.Query(query, req.Limit, offset)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			rep      m.RegionRes
			byteData []byte
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
			&rep.RepublicID,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.Region)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Regions = append(res.Regions, rep)
	}
	return &res, nil
}

func (r *RegionRepo) SearchRegion(req *m.SearchUserByName) (*m.RegionsList, error) {

	var (
		err        error
		res        m.RegionsList
		totalCount int
		offset     int
	)

	offset = (req.Page - 1) * req.Limit

	err = r.db.QueryRow(`SELECT COUNT(*) FROM Regions`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
	SELECT id, name, data, republic_id FROM regions
    WHERE 
        EXISTS (
            SELECT 1
            FROM jsonb_each_text(data) AS obj(key, value)
            WHERE value LIKE '%' || $1 || '%'
        )
    ORDER BY id DESC
    LIMIT $2 OFFSET $3`

	rows, err := r.db.Query(query, req.Name, req.Limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			byteData []byte
			rep      m.RegionRes
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
			&rep.RepublicID,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.Region)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Regions = append(res.Regions, rep)
	}

	return &res, nil
}

func (r *RegionRepo) UpdateRegion(req *m.RegionUpdateData) error {

	Region, err := r.GetRegionById(&req.ID)
	if err != nil {
		log.Println("error while getting Region data : ", err)
		return err
	}

	Region.Region[req.Language] = req.Name
	jsonData, err := json.Marshal(Region.Region)
	if err != nil {
		log.Println("errro while marshaling data : ", err)
		return err
	}

	query := `
		UPDATE 
			regions 
		SET
			name = $1,
			data = $2,
			republic_id = $3
		WHERE 
			id = $4
	`
	_, err = r.db.Exec(query, req.Title, jsonData, req.RepublicID, req.ID)
	if err != nil {
		return err
	}

	return nil
}

func (r *RegionRepo) DeleteRegion(id *int) error {

	query := `
		DELETE FROM regions WHERE id = $1
	`
	_, err := r.db.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}
