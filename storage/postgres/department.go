package postgres

import (
	"database/sql"
	"encoding/json"
	"log"

	"github.com/jmoiron/sqlx"
	m "gitlab.com/university/models"
)

type DepartmentRepo struct {
	db *sqlx.DB
}

func NewDepartmentRepo(db *sqlx.DB) *DepartmentRepo {
	return &DepartmentRepo{
		db: db,
	}
}

func (d *DepartmentRepo) CreateDepartment(req m.DepartmentReq) (*m.DepartmentRes, error) {
	var (
		err      error
		res      m.DepartmentRes
		id       sql.NullInt32
		byteData []byte
	)

	mpData := make(map[string]string)

	tz, err := d.db.Begin()
	if err != nil {
		log.Println("error while beginning transaction:", err)
		return nil, err
	}

	query := `
	SELECT 
		id
	FROM 
		departments
	WHERE 
		name = $1
	`

	err = tz.QueryRow(query, req.Title).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			mpData[req.Language] = req.Name
			jsonData, err := json.Marshal(mpData)
			if err != nil {
				tz.Rollback()
				log.Println("errro while marshaling data : ", err)
				return nil, err
			}
			query = `
			INSERT INTO departments(
				name,
				data
			)VALUES(
				$1, $2
			)RETURNING
				id,
				name,
				data
			`
			row := tz.QueryRow(query, req.Title, jsonData)
			err = row.Scan(&res.ID, &res.Title, &byteData)
			if err != nil {
				tz.Rollback()
				return nil, err
			}

			err = json.Unmarshal(byteData, &res.Department)
			if err != nil {
				tz.Rollback()
				log.Println("errro while umarshaling data : ", err)
				return nil, err
			}

		} else {
			tz.Rollback()
			log.Println("error while checking department exsistance : ", err)
			return nil, err
		}

	} else if id.Valid {
		reqID := int(id.Int32)
		Department, err := d.GetDepartmentById(&reqID)
		if err != nil {
			tz.Rollback()
			log.Println("error while getting Department data : ", err)
			return nil, err
		}

		Department.Department[req.Language] = req.Name
		jsonData, err := json.Marshal(Department.Department)
		if err != nil {
			tz.Rollback()
			log.Println("errro while marshaling data : ", err)
			return nil, err
		}

		query = `
		UPDATE departments SET data = $1
		WHERE id = $2
		RETURNING id, name, data
		`
		row := d.db.QueryRow(query, jsonData, reqID)

		err = row.Scan(&res.ID, &res.Title, &byteData)
		if err != nil {
			tz.Rollback()
			log.Println("errro while scanning rows : ", err)
			return nil, err
		}

		err = json.Unmarshal(byteData, &res.Department)
		if err != nil {
			tz.Rollback()
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

	}

	err = tz.Commit()
	if err != nil {
		log.Println("errro while committing transaction : ", err)
		return nil, err
	}

	return &res, nil
}

func (d *DepartmentRepo) GetDepartmentById(id *int) (*m.DepartmentRes, error) {
	var (
		err      error
		res      m.DepartmentRes
		byteData []byte
	)

	query := `
		SELECT 
			id,
			name,
			data
		FROM 
			departments
		WHERE 
			id = $1
	`

	err = d.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.Title,
		&byteData,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.Department)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (d *DepartmentRepo) GetDepartmentByName(name *string) (*m.DepartmentRes, error) {
	var (
		err      error
		res      m.DepartmentRes
		byteData []byte
	)

	query := `
	SELECT 
		id,
		name,
		data
	FROM 
		departments
	WHERE 
		name = $1
	`
	err = d.db.QueryRow(query, name).Scan(
		&res.ID,
		&res.Title,
		&byteData,
	)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &res.Department)
	if err != nil {
		log.Println("errro while umarshaling data : ", err)
		return nil, err
	}

	return &res, nil
}

func (d *DepartmentRepo) GetAllDepartments(req *m.GetAllRequest) (*m.DepartmentsList, error) {

	var (
		err        error
		res        m.DepartmentsList
		totalCount int
	)

	offset := (req.Page - 1) * req.Limit

	err = d.db.QueryRow(`SELECT COUNT(*) FROM departments`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
	SELECT 
		id,
		name,
		data
	FROM 
		departments
	LIMIT 
		$1 OFFSET $2`

	rows, err := d.db.Query(query, req.Limit, offset)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			rep      m.DepartmentRes
			byteData []byte
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.Department)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Departments = append(res.Departments, rep)
	}
	return &res, nil
}

func (d *DepartmentRepo) SearchDepartment(req *m.SearchUserByName) (*m.DepartmentsList, error) {

	var (
		err        error
		res        m.DepartmentsList
		totalCount int
		offset     int
	)

	offset = (req.Page - 1) * req.Limit

	err = d.db.QueryRow(`SELECT COUNT(*) FROM departments`).Scan(&totalCount)
	if err != nil {
		return nil, err
	}

	res.TotalCount = totalCount

	query := `
	SELECT id, name, data FROM departments
    WHERE 
        EXISTS (
            SELECT 1
            FROM jsonb_each_text(data) AS obj(key, value)
            WHERE value LIKE '%' || $1 || '%'
        )
    ORDER BY id DESC
    LIMIT $2 OFFSET $3`

	rows, err := d.db.Query(query, req.Name, req.Limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			byteData []byte
			rep      m.DepartmentRes
		)
		err = rows.Scan(
			&rep.ID,
			&rep.Title,
			&byteData,
		)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(byteData, &rep.Department)
		if err != nil {
			log.Println("errro while umarshaling data : ", err)
			return nil, err
		}

		res.Departments = append(res.Departments, rep)
	}

	return &res, nil
}

func (d *DepartmentRepo) UpdateDepartment(req *m.DepartmentUpdateData) error {

	Department, err := d.GetDepartmentById(&req.ID)
	if err != nil {
		log.Println("error while getting department data : ", err)
		return err
	}

	Department.Department[req.Language] = req.Name
	jsonData, err := json.Marshal(Department.Department)
	if err != nil {
		log.Println("errro while marshaling data : ", err)
		return err
	}

	query := `
		UPDATE 
			departments 
		SET
			data = $1
		WHERE 
			id = $2
	`
	_, err = d.db.Exec(query, jsonData, req.ID)
	if err != nil {
		return err
	}

	return nil
}

func (d *DepartmentRepo) DeleteDepartment(id *int) error {

	query := `
		DELETE FROM departments WHERE id = $1
	`
	_, err := d.db.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}
