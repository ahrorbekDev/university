package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/university/storage/postgres"
	"gitlab.com/university/storage/repo"
)

type IStorage interface {
	Admin() repo.AdminStorageI
	User() repo.UserStorageI
	Republic() repo.RepublicStorageI
	Region() repo.RegionStorageI
	District() repo.DistrictStorageI
	Department() repo.DepartmentStorageI
	Position() repo.PositionStorageI
}

type storagePg struct {
	db             *sqlx.DB
	userRepo       repo.UserStorageI
	adminRepo      repo.AdminStorageI
	republicRepo   repo.RepublicStorageI
	regionRepo     repo.RegionStorageI
	dirsrictRepo   repo.DistrictStorageI
	departmentRepo repo.DepartmentStorageI
	positionRepo   repo.PositionStorageI
}

func NewStoragePg(db *sqlx.DB) *storagePg {
	return &storagePg{
		db:             db,
		adminRepo:      postgres.NewAdminRepo(db),
		userRepo:       postgres.NewUserRepo(db),
		republicRepo:   postgres.NewRepublicRepo(db),
		regionRepo:     postgres.NewRegionRepo(db),
		dirsrictRepo:   postgres.NewDistrictRepo(db),
		departmentRepo: postgres.NewDepartmentRepo(db),
		positionRepo:   postgres.NewPositionRepo(db),
	}
}

func (s storagePg) Admin() repo.AdminStorageI {
	return s.adminRepo
}

func (s storagePg) User() repo.UserStorageI {
	return s.userRepo
}

func (s storagePg) Republic() repo.RepublicStorageI {
	return s.republicRepo
}

func (s storagePg) Region() repo.RegionStorageI {
	return s.regionRepo
}

func (s storagePg) District() repo.DistrictStorageI {
	return s.dirsrictRepo
}

func (s storagePg) Department() repo.DepartmentStorageI {
	return s.departmentRepo
}

func (s storagePg) Position() repo.PositionStorageI {
	return s.positionRepo
}
