package middleware

import (
	"log"
	"net/http"
	"strings"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"gitlab.com/university/api/token"
	"gitlab.com/university/config"
	"gitlab.com/university/models"
)

type JwtRoleAuth struct {
	enforcer   *casbin.Enforcer
	cfg        config.Config
	jwtHandler token.JWTHandler
}

func NewAuth(enforce *casbin.Enforcer, jwtHandler token.JWTHandler, cfg config.Config) gin.HandlerFunc {
	auth := JwtRoleAuth{
		enforcer:   enforce,
		cfg:        cfg,
		jwtHandler: jwtHandler,
	}

	return func(ctx *gin.Context) {
		allow, err := auth.CheckPermission(ctx.Request)

		if err != nil {
			valid, _ := err.(jwt.ValidationError)
			if valid.Errors == jwt.ValidationErrorExpired {
				auth.RequireRefresh(ctx)
			} else {
				auth.RequirePermission(ctx)
			}
		} else if !allow {
			auth.RequirePermission(ctx)
		}
	}

}

func (a *JwtRoleAuth) GetRole(r *http.Request) (string, error) {
	var (
		claims jwt.MapClaims
		err    error
	)
	jwtToken := r.Header.Get("Authorization")
	if jwtToken == "" {
		return "unauthorized", nil
	} else if strings.Contains(jwtToken, "Basic") {
		return "unauthorized", nil
	}
	a.jwtHandler.Token = jwtToken
	a.jwtHandler.SigningKey = a.cfg.SigninKey
	claims, err = a.jwtHandler.ExtractClaims()

	if err != nil {
		log.Println("Error while extracting claims: ", err)
		return "unauthorized", err
	}

	return claims["role"].(string), nil
}

func (a *JwtRoleAuth) CheckPermission(r *http.Request) (bool, error) {
	role, err := a.GetRole(r)
	if err != nil {
		log.Println("Error while getting role from token: ", err)
		return false, err
	}
	method := r.Method
	path := r.URL.Path

	allowed, err := a.enforcer.Enforce(role, path, method)
	if err != nil {
		log.Println("Error while comparing role from csv list: ", err)
		return false, err
	}

	return allowed, nil
}

func (a *JwtRoleAuth) InvalidToken(c *gin.Context) {
	c.AbortWithStatusJSON(http.StatusForbidden, models.ErrorResponse{
		Error: "Invalid token !!!",
	})
}

func (a *JwtRoleAuth) RequirePermission(c *gin.Context) {
	c.AbortWithStatusJSON(http.StatusForbidden, models.ErrorResponse{
		Error: "Permission denied",
	})
}

func (a *JwtRoleAuth) RequireRefresh(c *gin.Context) {
	c.AbortWithStatusJSON(401, models.ErrorResponse{
		Error: "Access token expired",
	})
}
