package api

import (
	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/util"

	"github.com/gin-contrib/cors"
	t "gitlab.com/university/api/token"
	"gitlab.com/university/pkg/logger"
	"gitlab.com/university/storage"

	v1 "gitlab.com/university/api/handlers/v1"
	"gitlab.com/university/config"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/university/api/docs"

	"github.com/gin-gonic/gin"
)

// Option ...
type Option struct {
	Logger   *logger.Logger
	Cfg      *config.Config
	Storage  storage.IStorage
	InMemory storage.InMemoryStorageI
}

// New ...
// @title	Project: University by Ahrorbek
// @version 1.0
// @BasePath  /v1
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func New(option Option) *gin.Engine {
	casbinEnforcer, err := casbin.NewEnforcer(option.Cfg.AuthConfigPath, option.Cfg.CSVFilePath)
	if err != nil {
		option.Logger.Error("casbin enforcer error", err)
		panic(err)
	}

	err = casbinEnforcer.LoadPolicy()
	if err != nil {
		option.Logger.Error("casbin error load policy", err)
		panic(err)
	}

	casbinEnforcer.GetRoleManager().AddMatchingFunc("keyMatch", util.KeyMatch)
	casbinEnforcer.GetRoleManager().AddMatchingFunc("keyMatch3", util.KeyMatch3)

	jwtHandler := t.JWTHandler{
		SigningKey: option.Cfg.SigninKey,
		Log:        option.Logger,
	}

	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:     option.Logger,
		Cfg:        *option.Cfg,
		Storage:    option.Storage,
		InMemory:   option.InMemory,
		JWTHandler: jwtHandler,
		Enforser:   casbinEnforcer,
	})

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowHeaders = append(corsConfig.AllowHeaders, "*")
	corsConfig.AllowBrowserExtensions = true
	corsConfig.AllowMethods = []string{"*"}
	router.Use(cors.New(corsConfig))

	// router.Use(middleware.NewAuth(casbinEnforcer, jwtHandler, *option.Cfg))
	api := router.Group("/v1")

	// Admin
	admin := api.Group("/admin")
	admin.POST("/register", handlerV1.AdminRegister)
	admin.POST("/verify", handlerV1.Verify)
	admin.POST("/login", handlerV1.Login)
	admin.POST("/refresh-accesstoken/:token", handlerV1.RefreshAccessToken)
	admin.POST("/forgot-password", handlerV1.ForgotPassword)
	admin.POST("/verify-forgot-password", handlerV1.VerifyForgotPassword)
	admin.POST("/update-password", handlerV1.UpdatePassword)
	admin.DELETE("/delete/:id", handlerV1.DeleteAccount)

	// User
	user := api.Group("/user")
	user.POST("/login", handlerV1.LoginUser)
	user.POST("/register", handlerV1.RegisterUser)
	user.POST("", handlerV1.CreateUser)
	user.GET("/id", handlerV1.GetUserById)
	user.GET("/username", handlerV1.GetUserByUsername)
	user.GET("/all", handlerV1.GetAllUsers)
	user.GET("/search", handlerV1.SearchUser)
	user.PUT("", handlerV1.UpdateUser)
	user.DELETE("", handlerV1.DeleteUser)

	// Republic
	republic := api.Group("/republic")
	republic.POST("", handlerV1.CreateRepublic)
	republic.GET("", handlerV1.GetRepublicById)
	republic.GET("/all", handlerV1.GetAllRepublic)
	republic.GET("/name", handlerV1.GetRepublicByName)
	republic.GET("/search", handlerV1.SearchRepublic)
	republic.PUT("", handlerV1.UpdateRepublic)
	republic.DELETE("", handlerV1.DeleteRepublic)

	// Region
	region := api.Group("/region")
	region.POST("", handlerV1.CreateRegion)
	region.GET("", handlerV1.GetRegionById)
	region.GET("/all", handlerV1.GetAllRegion)
	region.GET("/name", handlerV1.GetRegionByName)
	region.GET("/search", handlerV1.SearchRegion)
	region.PUT("", handlerV1.UpdateRegion)
	region.DELETE("", handlerV1.DeleteRegion)

	// District
	district := api.Group("/district")
	district.POST("", handlerV1.CreateDistrict)
	district.GET("", handlerV1.GetDistrictById)
	district.GET("/all", handlerV1.GetAllDistrict)
	district.GET("/name", handlerV1.GetDistrictByName)
	district.GET("/search", handlerV1.SearchDistrict)
	district.PUT("", handlerV1.UpdateDistrict)
	district.DELETE("", handlerV1.DeleteDistrict)

	// Department
	department := api.Group("/department")
	department.POST("", handlerV1.CreateDepartment)
	department.GET("", handlerV1.GetDepartmentById)
	department.GET("/all", handlerV1.GetAllDepartment)
	department.GET("/name", handlerV1.GetDepartmentByName)
	department.GET("/search", handlerV1.SearchDepartment)
	department.PUT("", handlerV1.UpdateDepartment)
	department.DELETE("", handlerV1.DeleteDepartment)

	// Position
	position := api.Group("/position")
	position.POST("", handlerV1.CreatePosition)
	position.GET("", handlerV1.GetPositionById)
	position.GET("/all", handlerV1.GetAllPosition)
	position.GET("/name", handlerV1.GetPositionByName)
	position.GET("/search", handlerV1.SearchPosition)
	position.PUT("", handlerV1.UpdatePosition)
	position.DELETE("", handlerV1.DeletePosition)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router
}
