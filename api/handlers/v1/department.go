package v1

import (
	"database/sql"
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	m "gitlab.com/university/models"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Summary 			CREATE DEPARTMENT
// @Description		 	This api create a department
// @Tags 				DEPARTMENT
// /@Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param body 			body models.DepartmentReq true "Create Department"
// @Success 201 		{object} models.DepartmentRes
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/department [post]
func (h *handlerV1) CreateDepartment(c *gin.Context) {
	var (
		body        m.DepartmentReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	response, err := h.storage.Department().CreateDepartment(m.DepartmentReq{
		Title:    strings.ToLower(body.Title),
		Name:     strings.ToLower(body.Name),
		Language: strings.ToLower(body.Language),
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// @Summary 			GET ALL DEPARTMENT
// @Description 		This method for get all departments
// @Tags 				DEPARTMENT
// /@Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Success 200 		{object} models.DepartmentsList
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/department/all [get]
func (h *handlerV1) GetAllDepartment(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.Department().GetAllDepartments(&m.GetAllRequest{
		Page:  intPage,
		Limit: intLimit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET DEPARTMENT
// @Description 		This api gets a department by ID
// /@Security    		BearerAuth
// @Tags 				DEPARTMENT
// @Accept 				json
// @Produce 			json
// @Param 				id query string true "ID"
// @Success 200 		{object} models.DepartmentRes
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/department [get]
func (h *handlerV1) GetDepartmentById(c *gin.Context) {

	id := c.Query("id")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	response, err := h.storage.Department().GetDepartmentById(&intId)

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET DEPARTMENT
// @Description 		This api gets a department by Name
// /@Security    		BearerAuth
// @Tags 				DEPARTMENT
// @Accept 				json
// @Produce 			json
// @Param 				name query string true "Name"
// @Success 200 		{object} models.DepartmentRes
// @Failure 404			{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/department/name [get]
func (h *handlerV1) GetDepartmentByName(c *gin.Context) {

	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	name = strings.ToLower(name)

	response, err := h.storage.Department().GetDepartmentByName(&name)

	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			SEARCH DEPARTMENT
// @Description 		This api search a departments
// /@Security    		BearerAuth
// @Tags 				DEPARTMENT
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Param 				name query string true "Name"
// @Success 200 		{object} models.DepartmentsList
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/department/search [get]
func (h *handlerV1) SearchDepartment(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")
	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" || name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.Department().SearchDepartment(&m.SearchUserByName{
		Page:  intPage,
		Limit: intLimit,
		Name:  strings.ToLower(name),
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			UPDATE DEPARTMENT
// @Description 		This api updates a department
// /@Security    		BearerAuth
// @Tags 				DEPARTMENT
// @Accept 				json
// @Produce 			json
// @Param body 			body models.DepartmentUpdateData true "Update Department"
// @Success 200 		string string
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/department [put]
func (h *handlerV1) UpdateDepartment(c *gin.Context) {
	var (
		body        m.DepartmentUpdateData
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }
	// body.OwnerId = ownerId

	err = h.storage.Department().UpdateDepartment(&m.DepartmentUpdateData{
		ID:       body.ID,
		Title:    strings.ToLower(body.Title),
		Name:     strings.ToLower(body.Name),
		Language: strings.ToLower(body.Language),
	})

	if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, errorResponse(errors.New("Department not found")))
		return
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "successfully updated",
	})
}

// @Summary      		DELETE DEPARTMENT
// @Tags         		DEPARTMENT
// @Description  		This method for deleteing department
// /@Security    		BearerAuth
// @Accept 		 		json
// @Produce 		    json
// @Param 				id query string true "id"
// @Success 200 		string string
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500		    string Error models.ErrorResponse
// @Router 				/department [delete]
func (h *handlerV1) DeleteDepartment(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("error : ID parameter is required")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid id")))
		return
	}

	err = h.storage.Department().DeleteDepartment(&intId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusOK, "Deleted succesfully")
}
