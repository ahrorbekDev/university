package v1

import (
	"database/sql"
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	m "gitlab.com/university/models"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Summary 			CREATE POSITION
// @Description		 	This api create a position
// @Tags 				POSITION
// /@Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param body 			body models.PositionReq true "Create Position"
// @Success 201 		{object} models.PositionRes
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/position [post]
func (h *handlerV1) CreatePosition(c *gin.Context) {
	var (
		body        m.PositionReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	response, err := h.storage.Position().CreatePosition(m.PositionReq{
		Title:    strings.ToLower(body.Title),
		Name:     strings.ToLower(body.Name),
		Language: strings.ToLower(body.Language),
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// @Summary 			GET ALL POSITIONS
// @Description 		This method for get all positions
// @Tags 				POSITION
// /@Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Success 200 		{object} models.PositionsList
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/position/all [get]
func (h *handlerV1) GetAllPosition(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.Position().GetAllPositions(&m.GetAllRequest{
		Page:  intPage,
		Limit: intLimit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET POSITION
// @Description 		This api gets a position by ID
// /@Security    		BearerAuth
// @Tags 				POSITION
// @Accept 				json
// @Produce 			json
// @Param 				id query string true "ID"
// @Success 200 		{object} models.PositionRes
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/position [get]
func (h *handlerV1) GetPositionById(c *gin.Context) {

	id := c.Query("id")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	response, err := h.storage.Position().GetPositionById(&intId)

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET POSITION
// @Description 		This api gets a position by Name
// /@Security    		BearerAuth
// @Tags 				POSITION
// @Accept 				json
// @Produce 			json
// @Param 				name query string true "Name"
// @Success 200 		{object} models.PositionRes
// @Failure 404			{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/position/name [get]
func (h *handlerV1) GetPositionByName(c *gin.Context) {

	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	name = strings.ToLower(name)

	response, err := h.storage.Position().GetPositionByName(&name)

	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			SEARCH POSITION
// @Description 		This api search a positions
// /@Security    		BearerAuth
// @Tags 				POSITION
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Param 				name query string true "Name"
// @Success 200 		{object} models.PositionsList
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/position/search [get]
func (h *handlerV1) SearchPosition(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")
	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" || name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.Position().SearchPosition(&m.SearchUserByName{
		Page:  intPage,
		Limit: intLimit,
		Name:  strings.ToLower(name),
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			UPDATE POSITION
// @Description 		This api updates a position
// /@Security    		BearerAuth
// @Tags 				POSITION
// @Accept 				json
// @Produce 			json
// @Param body 			body models.PositionUpdateData true "Update Position"
// @Success 200 		string string
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/position [put]
func (h *handlerV1) UpdatePosition(c *gin.Context) {
	var (
		body        m.PositionUpdateData
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }
	// body.OwnerId = ownerId

	err = h.storage.Position().UpdatePosition(&m.PositionUpdateData{
		ID:       body.ID,
		Name:     strings.ToLower(body.Name),
		Language: strings.ToLower(body.Language),
	})

	if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, errorResponse(errors.New("Position not found")))
		return
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "successfully updated",
	})
}

// @Summary      		DELETE POSITION
// @Tags         		POSITION
// @Description  		This method for deleteing position
// /@Security    		BearerAuth
// @Accept 		 		json
// @Produce 		    json
// @Param 				id query string true "id"
// @Success 200 		string string
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500		    string Error models.ErrorResponse
// @Router 				/position [delete]
func (h *handlerV1) DeletePosition(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("error : ID parameter is required")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid id")))
		return
	}

	err = h.storage.Position().DeletePosition(&intId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusOK, "Deleted succesfully")
}
