package v1

import (
	"errors"

	"github.com/casbin/casbin/v2"
	t "gitlab.com/university/api/token"
	"gitlab.com/university/config"
	"gitlab.com/university/models"
	"gitlab.com/university/pkg/logger"
	"gitlab.com/university/storage"
)

var (
	ErrWrongEmailOrPass = errors.New("wrong email or password")
	ErrEmailExists      = errors.New("email already exists")
	ErrUserNotVerified  = errors.New("user not verified")
	ErrIncorrectCode    = errors.New("incorrect verification code")
	ErrCodeExpired      = errors.New("verification code has been expired")
	ErrForbidden        = errors.New("forbidden")
)

type handlerV1 struct {
	cfg        config.Config
	storage    storage.IStorage
	inMemory   storage.InMemoryStorageI
	jwtHandler t.JWTHandler
	log        *logger.Logger
	enforcer   *casbin.Enforcer
}

type HandlerV1Config struct {
	Logger     *logger.Logger
	Cfg        config.Config
	Storage    storage.IStorage
	InMemory   storage.InMemoryStorageI
	JWTHandler t.JWTHandler
	Enforser   *casbin.Enforcer
}

func New(c *HandlerV1Config) *handlerV1 {

	return &handlerV1{
		inMemory:   c.InMemory,
		storage:    c.Storage,
		cfg:        c.Cfg,
		jwtHandler: c.JWTHandler,
		log:        c.Logger,
		enforcer:   c.Enforser,
	}
}

func errorResponse(err error) *models.ErrorResponse {
	return &models.ErrorResponse{
		Error: err.Error(),
	}
}
