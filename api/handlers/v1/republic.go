package v1

import (
	"database/sql"
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	m "gitlab.com/university/models"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Summary 			CREATE REPUBLIC
// @Description		 	This api create a republic
// /@Security    		BearerAuth
// @Tags 				REPUBLIC
// @Accept 				json
// @Produce 			json
// @Param body 			body models.RepublicReq true "Create Republic"
// @Success 201 		{object} models.RepublicRes
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/republic [post]
func (h *handlerV1) CreateRepublic(c *gin.Context) {
	var (
		body        m.RepublicReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	response, err := h.storage.Republic().CreateRepublic(m.RepublicReq{
		Title:    strings.ToLower(body.Title),
		Name:     strings.ToLower(body.Name),
		Language: strings.ToLower(body.Language),
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// @Summary 			GET ALL REPUBLICS
// @Tags 				REPUBLIC
// @Description 		This method for get all republics
// /@Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Success 200 		{object} models.RepublicsList
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/republic/all [get]
func (h *handlerV1) GetAllRepublic(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.Republic().GetAllRepublics(&m.GetAllRequest{
		Page:  intPage,
		Limit: intLimit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET REPUBLIC
// @Description 		This api gets a republics by ID
// /@Security    		BearerAuth
// @Tags 				REPUBLIC
// @Accept 				json
// @Produce 			json
// @Param 				id query string true "ID"
// @Success 200 		{object} models.RepublicRes
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/republic [get]
func (h *handlerV1) GetRepublicById(c *gin.Context) {

	id := c.Query("id")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	response, err := h.storage.Republic().GetRepublicById(&intId)

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET REPUBLIC
// @Description 		This api gets a republic by Name
// /@Security    		BearerAuth
// @Tags 				REPUBLIC
// @Accept 				json
// @Produce 			json
// @Param 				name query string true "Name"
// @Success 200 		{object} models.RepublicRes
// @Failure 404			{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/republic/name [get]
func (h *handlerV1) GetRepublicByName(c *gin.Context) {

	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	name = strings.ToLower(name)

	response, err := h.storage.Republic().GetRepublicByName(&name)

	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			SEARCH REPUBLICS
// @Description 		This api search a republics
// /@Security    		BearerAuth
// @Tags 				REPUBLIC
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Param 				name query string true "Name"
// @Success 200 		{object} models.RepublicsList
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/republic/search [get]
func (h *handlerV1) SearchRepublic(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")
	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" || name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.Republic().SearchRepublic(&m.SearchUserByName{
		Page:  intPage,
		Limit: intLimit,
		Name:  strings.ToLower(name),
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			UPDATE REPUBLIC
// @Description 		This api updates a republic
// /@Security    		BearerAuth
// @Tags 				REPUBLIC
// @Accept 				json
// @Produce 			json
// @Param body 			body models.RepublicUpdateData true "Update Republic"
// @Success 200 		string string
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/republic [put]
func (h *handlerV1) UpdateRepublic(c *gin.Context) {
	var (
		body        m.RepublicUpdateData
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }
	// body.OwnerId = ownerId

	err = h.storage.Republic().UpdateRepublic(&m.RepublicUpdateData{
		ID:       body.ID,
		Title:    strings.ToLower(body.Title),
		Name:     strings.ToLower(body.Name),
		Language: strings.ToLower(body.Language),
	})

	if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, errorResponse(errors.New("republic not found")))
		return
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "successfully updated",
	})
}

// @Summary      		DELETE REPUBLIC
// @Tags         		REPUBLIC
// @Description  		this method for deleteing republic
// /@Security    		BearerAuth
// @Accept 		 		json
// @Produce 		    json
// @Param 				id query string true "id"
// @Success 200 		string string
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500		    string Error models.ErrorResponse
// @Router 				/republic [delete]
func (h *handlerV1) DeleteRepublic(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("error : ID parameter is required")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid id")))
		return
	}

	err = h.storage.Republic().DeleteRepublic(&intId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusOK, "Deleted succesfully")
}
