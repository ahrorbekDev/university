package v1

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	t "gitlab.com/university/api/token"
	"gitlab.com/university/models"
	etc "gitlab.com/university/pkg/etc"
)

// @Router 			/user/register [post]
// @Summary 		USER REGISTER
// @Description 	Register
// @Tags 			USER
// @Accept 			json
// @Produce 		json
// @Param 			admin body models.UserReq true "Register User"
// @Success 		200 {object} models.ResponseOK
// @Failure 		400 {object} models.ErrorResponse
// @Failure 		500 {object} models.ErrorResponse
func (h *handlerV1) RegisterUser(c *gin.Context) {
	var (
		req models.UserReq
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	// password validating
	isValid := ValidatePassword(req.Password)
	if !isValid {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid password")))
		return
	}	

	// checking if user already exsists
	_, err = h.storage.User().GetUserByUsername(&req.Username)
	if !errors.Is(err, sql.ErrNoRows) {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("user already exsists")))
		return
	}

	hashedPassword, err := etc.HashPassword(req.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	h.jwtHandler = t.JWTHandler{
		Sub:        req.Username,
		Role:       req.Role,
		SigningKey: h.cfg.SigninKey,
		Log:        h.log,
		Timeout:    480,
	}

	access, refresh, err := h.jwtHandler.GenerateAuthJWT()
	if h.HandleResponse(c, err, http.StatusInternalServerError, InternalServerError, "UserRegister: jwthandler.GenerateAuthJWT()", nil) {
		return
	}

	res, err := h.storage.User().CreateUser(&models.UserReq{
		FullName:        req.FullName,
		Username:        req.Username,
		Password:        hashedPassword,
		Role:            req.Role,
		BirthDate:       req.BirthDate,
		Avatar:          req.Avatar,
		BirthDistrictID: req.BirthDistrictID,
	})

	res.AccessToken = access
	res.RefreshToken = refresh

	c.JSON(http.StatusOK, res)
}

// @Router 			/user/login [post]
// @Summary 		USER LOGIN
// @Description 	Login by email and password
// @Tags 			USER
// @Accept 			json
// @Produce 		json
// @Param 			data body models.UserLogin true "Login"
// @Success 		200 {object} models.UserRes
// @Failure 		400 {object} models.ErrorResponse
// @Failure 		404 {object} models.ErrorResponse
// @Failure 		500 {object} models.ErrorResponse
func (h *handlerV1) LoginUser(c *gin.Context) {
	var (
		req models.UserLogin
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.storage.User().GetUserByUsername(&req.Username)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			c.JSON(http.StatusNotFound, errorResponse(ErrWrongEmailOrPass))
			return
		}

		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	fmt.Println(result.Password)

	err = etc.CheckPassword(req.Password, result.Password)
	if err != nil {
		h.HandleResponse(c, fmt.Errorf(BadRequest), http.StatusBadRequest, BadRequest, "incorrect password", nil)
		return
	}

	h.jwtHandler = t.JWTHandler{
		Sub:        result.Username,
		Role:       result.Role,
		SigningKey: h.cfg.SigninKey,
		Log:        h.log,
		Timeout:    480,
	}

	access, refresh, err := h.jwtHandler.GenerateAuthJWT()
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result.AccessToken = access
	result.RefreshToken = refresh

	c.JSON(http.StatusOK, result)
}
