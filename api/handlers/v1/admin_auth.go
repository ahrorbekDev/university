package v1

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	m "gitlab.com/university/models"

	"github.com/gin-gonic/gin"
	t "gitlab.com/university/api/token"
	"gitlab.com/university/models"
	emailPkg "gitlab.com/university/pkg/email"
	etc "gitlab.com/university/pkg/etc"
)

// @Router 			/admin/register [post]
// @Summary 		Register
// @Description 	Register
// @Tags 			ADMIN
// @Accept 			json
// @Produce 		json
// @Param 			admin body models.RegisterRequest true "RegisterRequest"
// @Success 		200 {object} models.ResponseOK
// @Failure 		400 {object} models.ErrorResponse
// @Failure 		500 {object} models.ErrorResponse
func (h *handlerV1) AdminRegister(c *gin.Context) {
	var (
		req models.RegisterRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	// validate password
	isValid := ValidatePassword(req.Password)
	if !isValid {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid password")))
		return
	}

	// validate phoneNumber
	isValid = ValidatePhoneNumber(req.PhoneNumber)
	if !isValid {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid phone number")))
		return
	}

	_, err = h.storage.Admin().GetByEmail(c, req.Email)
	if !errors.Is(err, sql.ErrNoRows) {
		c.JSON(http.StatusBadRequest, errorResponse(ErrEmailExists))
		return
	}

	hashedPassword, err := etc.HashPassword(req.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	admin := m.AdminReq{
		FullName:    req.Full_Name,
		PhoneNumber: req.PhoneNumber,
		Email:       req.Email,
		Password:    hashedPassword,
		Role:        "admin",
	}

	adminData, err := json.Marshal(admin)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.inMemory.Set("email_"+admin.Email, string(adminData), 5*time.Minute)

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	go func() {
		err := h.sendVerificationCode(RegisterCodeKey, req.Email)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}()

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "Verification code has been sent!",
	})
}

func (h *handlerV1) sendVerificationCode(key, email string) error {
	code, err := etc.GenerateRandomCode(6)
	if err != nil {
		return err
	}

	err = h.inMemory.Set(key+email, code, time.Minute)
	if err != nil {
		return err
	}

	err = emailPkg.SendEmail(&h.cfg, &emailPkg.SendEmailRequest{
		To:      []string{email},
		Subject: "Verification email",
		Body: map[string]string{
			"code": code,
		},
		Type: emailPkg.VerificationEmail,
	})
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

// @Router 		/admin/verify [post]
// @Summary 	Verify admin
// @Description Verify admin
// @Tags 		ADMIN
// @Accept 		json
// @Produce 	json
// @Param 		data body models.VerifyEmail true "Verify"
// @Success 	201 {object} models.AuthResponse
// @Failure 	500 {object} models.ErrorResponse
func (h *handlerV1) Verify(c *gin.Context) {
	var (
		req models.VerifyEmail
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	adminData, err := h.inMemory.Get("email_" + req.Email)
	if err != nil {
		c.JSON(http.StatusForbidden, errorResponse(err))
		return
	}

	var admin m.AdminReq
	err = json.Unmarshal([]byte(adminData), &admin)
	if err != nil {
		c.JSON(http.StatusForbidden, errorResponse(err))
		return
	}

	code, err := h.inMemory.Get(RegisterCodeKey + admin.Email)
	if err != nil {
		c.JSON(http.StatusForbidden, errorResponse(ErrCodeExpired))
		return
	}

	if req.Code != code {
		c.JSON(http.StatusForbidden, errorResponse(ErrIncorrectCode))
		return
	}

	admin.Role = "admin"

	h.jwtHandler = t.JWTHandler{
		Sub:        admin.Email,
		Role:       admin.Role,
		SigningKey: h.cfg.SigninKey,
		Log:        h.log,
		Timeout:    480,
	}

	ctxWithCancel, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	access, refresh, err := h.jwtHandler.GenerateAuthJWT()
	if h.HandleResponse(c, err, http.StatusInternalServerError, InternalServerError, "UserRegister: jwthandler.GenerateAuthJWT()", nil) {
		return
	}

	result, err := h.storage.Admin().Create(ctxWithCancel, &admin)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	result.AccessToken = access
	result.RefreshToken = refresh

	c.JSON(http.StatusCreated, result)
}

// @Router 		/admin/login [post]
// @Summary 	Login
// @Description Login by email and password
// @Tags 		ADMIN
// @Accept 		json
// @Produce 	json
// @Param 		data body models.LoginRequest true "Login"
// @Success 	200 {object} models.AdminRes
// @Failure 	400 {object} models.ErrorResponse
// @Failure 	404 {object} models.ErrorResponse
// @Failure 	500 {object} models.ErrorResponse
func (h *handlerV1) Login(c *gin.Context) {
	var (
		req models.LoginRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	ctxWithCancel, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	if err != nil {
		c.JSON(http.StatusForbidden, errorResponse(ErrWrongEmailOrPass))
		return
	}

	result, err := h.storage.Admin().GetByEmail(ctxWithCancel, req.Email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			c.JSON(http.StatusNotFound, errorResponse(ErrWrongEmailOrPass))
			return
		}

		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = etc.CheckPassword(req.Password, result.Password)
	if err != nil {
		h.HandleResponse(c, fmt.Errorf(BadRequest), http.StatusBadRequest, BadRequest, "incorrect password", nil)
		return
	}

	h.jwtHandler = t.JWTHandler{
		Sub:        result.Email,
		Role:       result.Role,
		SigningKey: h.cfg.SigninKey,
		Log:        h.log,
		Timeout:    480,
	}

	access, refresh, err := h.jwtHandler.GenerateAuthJWT()
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result.AccessToken = access
	result.RefreshToken = refresh

	c.JSON(http.StatusOK, result)
}

// @Router 		/admin/forgot-password [post]
// @Summary 	Forgot password
// @Description Forgot password
// @Tags 		ADMIN
// @Accept 		json
// @Produce 	json
// @Param 		data body models.ForgotPasswordRequest true "Data"
// @Success 	200 {object} models.ResponseOK
// @Failure 	500 {object} models.ErrorResponse
func (h *handlerV1) ForgotPassword(c *gin.Context) {
	var (
		req models.ForgotPasswordRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	_, err = h.storage.Admin().GetByEmail(c, req.Email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		}

		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	go func() {
		err := h.sendVerificationCode(ForgotPasswordKey, req.Email)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errorResponse(errors.New("failed to send verification code")))
			return
		}
	}()

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "Verification code has been sent!",
	})
}

// @Router 		/admin/verify-forgot-password [post]
// @Summary 	Verify forgot password
// @Description Verify forgot password
// @Tags 		ADMIN
// @Accept 		json
// @Produce 	json
// @Param 		data body models.VerifyRequest true "Data"
// @Success 	201 {object} models.AuthResponse
// @Failure 	500 {object} models.ErrorResponse
func (h *handlerV1) VerifyForgotPassword(c *gin.Context) {
	var (
		req models.VerifyRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	code, err := h.inMemory.Get(ForgotPasswordKey + req.Email)
	if err != nil {
		c.JSON(http.StatusForbidden, errorResponse(ErrCodeExpired))
		return
	}

	if req.Code != code {
		c.JSON(http.StatusForbidden, errorResponse(ErrIncorrectCode))
		return
	}

	hashedPassword, err := etc.HashPassword(req.NewPassword)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.storage.Admin().UpdatePassword(c, &m.UpdatePassword{
		Email:    req.Email,
		Password: hashedPassword,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.ResponseOK{Message: "Password succasfully updated!!!"})
}

// @Router 		/admin/update-password [post]
// @Summary 	Update password
// @Description Update password
// /@Security 	BearerAuth
// @Tags 		ADMIN
// @Accept 		json
// @Produce 	json
// @Param 		data body models.UpdatePasswordRequest true "Data"
// @Success 	201 {object} models.ResponseOK
// @Failure 	500 {object} models.ErrorResponse
func (h *handlerV1) UpdatePassword(c *gin.Context) {
	var (
		req models.UpdatePasswordRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	hashedPassword, err := etc.HashPassword(req.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.storage.Admin().UpdatePassword(c, &m.UpdatePassword{
		Email:    req.Email,
		Password: hashedPassword,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: "Password has been updated!",
	})
}

// @Router 		/admin/delete/{eamil} [delete]
// @Summary 	Delete account
// @Description Delete account with id
// /@Security 	BearerAuth
// @Tags 		ADMIN
// @Accept 		json
// @Produce 	json
// @Param  		eamil path string true "eamil"
// @Success 	200 {object} models.ResponseOK
// @Failure 	400 {object} models.ErrorResponse
// @Failure 	403 {object} models.StandartResponse
// @Failure 	500 {object} models.ErrorResponse
func (h *handlerV1) DeleteAccount(c *gin.Context) {

	email := c.Param("email")

	res, err := h.storage.Admin().GetByEmail(c, email)
	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(http.StatusBadRequest, errorResponse(errors.New("admin does not exsists")))
			return
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.storage.Admin().DeleteAccount(c, res.Id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "Successfully deleted",
	})
}
