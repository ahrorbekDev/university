package v1

import (
	"net/http"
	"strconv"
	"unicode"

	"github.com/gin-gonic/gin"
	t "gitlab.com/university/api/token"
	"gitlab.com/university/models"
)

const (
	RegisterCodeKey   = "register_code_"
	ForgotPasswordKey = "forgot_password_code_"
)

func ValidatePassword(password string) bool {
	// Check if password length is at least 6 characters
	if len(password) < 6 {
		return false
	}

	var hasLower, hasUpper bool
	for _, char := range password {
		// Check if password contains at least one lowercase letter
		if unicode.IsLower(char) {
			hasLower = true
		}
		// Check if password contains at least one uppercase letter
		if unicode.IsUpper(char) {
			hasUpper = true
		}
	}

	// Return true only if both lowercase and uppercase letters are present
	return hasLower && hasUpper
}

func ValidatePhoneNumber(phoneNumber string) bool {
	// Check if phone number is 13 characters long
	if len(phoneNumber) != 13 {
		return false
	}

	// Check if first 4 characters are "+998"
	prefix := "+998"
	if phoneNumber[:4] != prefix {
		return false
	}

	if _, err := strconv.Atoi(phoneNumber[4:]); err != nil {
		return false
	}

	// Return true if all conditions are met
	return true
}

// @Router 		/refresh-accesstoken/{token} [post]
// @Summary 	Creates new valid access-token
// @Description Refresh access-token
// @Tags 		AUTH
// @Accept 		json
// @Produce 	json
// @Param 		token path string true "Token"
// @Success 	200 {object} models.AccessToken
// @Failure 	400 {object} models.StandartResponse
// @Failure 	401 {object} models.ErrorResponse
// @Failure 	500 {object} models.ErrorResponse
func (h *handlerV1) RefreshAccessToken(c *gin.Context) {

	token := c.Param("token")

	claims, err := t.ExtractClaim(&h.cfg, token)
	if err != nil {
		c.AbortWithStatusJSON(401, models.StandartResponse{
			Status:  RefreshTokenExpired,
			Message: "Refresh token expired",
		})
		return
	}

	jwtHandler := t.JWTHandler{
		Sub:        claims["sub"].(string),
		Role:       claims["role"].(string),
		SigningKey: h.cfg.SigninKey,
		Log:        h.log,
		Timeout:    h.cfg.AccessTokenTimeOut,
	}

	access, _, err := jwtHandler.GenerateAuthJWT()
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.AccessToken{
		AccessToken: access,
	})
}
