package v1

import (
	"database/sql"
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	m "gitlab.com/university/models"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Summary 			CREATE REGION
// @Description		 	This api create a region
// @Tags 				REGION
// /@Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param body 			body models.RegionReq true "Create Region"
// @Success 201 		{object} models.RegionRes
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/region [post]
func (h *handlerV1) CreateRegion(c *gin.Context) {
	var (
		body        m.RegionReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	response, err := h.storage.Region().CreateRegion(m.RegionReq{
		Title:      strings.ToLower(body.Title),
		Name:       strings.ToLower(body.Name),
		Language:   strings.ToLower(body.Language),
		RepublicID: body.RepublicID,
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// @Summary 			GET ALL REGIONS
// @Description 		This method for get all regions
// @Tags 				REGION
// /@Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Success 200 		{object} models.RegionsList
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/region/all [get]
func (h *handlerV1) GetAllRegion(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.Region().GetAllRegions(&m.GetAllRequest{
		Page:  intPage,
		Limit: intLimit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET REGION
// @Description 		This api gets a region by ID
// /@Security    		BearerAuth
// @Tags 				REGION
// @Accept 				json
// @Produce 			json
// @Param 				id query string true "ID"
// @Success 200 		{object} models.RegionRes
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/region [get]
func (h *handlerV1) GetRegionById(c *gin.Context) {

	id := c.Query("id")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	response, err := h.storage.Region().GetRegionById(&intId)

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET REGION
// @Description 		This api gets a region by Name
// /@Security    		BearerAuth
// @Tags 				REGION
// @Accept 				json
// @Produce 			json
// @Param 				name query string true "Name"
// @Success 200 		{object} models.RegionRes
// @Failure 404			{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/region/name [get]
func (h *handlerV1) GetRegionByName(c *gin.Context) {

	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	name = strings.ToLower(name)

	response, err := h.storage.Region().GetRegionByName(&name)

	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			SEARCH REGIONS
// @Description 		This api search a regions
// /@Security    		BearerAuth
// @Tags 				REGION
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Param 				name query string true "Name"
// @Success 200 		{object} models.RegionsList
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/region/search [get]
func (h *handlerV1) SearchRegion(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")
	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" || name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.Region().SearchRegion(&m.SearchUserByName{
		Page:  intPage,
		Limit: intLimit,
		Name:  strings.ToLower(name),
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			UPDATE REGION
// @Description 		This api updates a region
// /@Security    		BearerAuth
// @Tags 				REGION
// @Accept 				json
// @Produce 			json
// @Param body 			body models.RegionUpdateData true "Update Region"
// @Success 200 		string string
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/region [put]
func (h *handlerV1) UpdateRegion(c *gin.Context) {
	var (
		body        m.RegionUpdateData
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }
	// body.OwnerId = ownerId

	err = h.storage.Region().UpdateRegion(&m.RegionUpdateData{
		ID:         body.ID,
		Title:      strings.ToLower(body.Title),
		Name:       strings.ToLower(body.Name),
		Language:   strings.ToLower(body.Language),
		RepublicID: body.RepublicID,
	})

	if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, errorResponse(errors.New("Region not found")))
		return
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "successfully updated",
	})
}

// @Summary      		DELETE REGION
// @Tags         		REGION
// @Description  		This method for deleteing region
// /@Security    		BearerAuth
// @Accept 		 		json
// @Produce 		    json
// @Param 				id query string true "id"
// @Success 200 		string string
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500		    string Error models.ErrorResponse
// @Router 				/region [delete]
func (h *handlerV1) DeleteRegion(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("error : ID parameter is required")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid id")))
		return
	}

	err = h.storage.Region().DeleteRegion(&intId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusOK, "Deleted succesfully")
}
