package v1

import (
	"database/sql"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	t "gitlab.com/university/api/token"
	m "gitlab.com/university/models"
	"gitlab.com/university/pkg/etc"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Summary 			CREATE USER
// @Description		 	This api creates a user
// /@Security    		BearerAuth
// @Tags 				USER
// @Accept 				json
// @Produce 			json
// @Param body 			body models.UserReq true "Create User"
// @Success 201 		{object} models.UserRes
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/user [post]
func (h *handlerV1) CreateUser(c *gin.Context) {
	var (
		body        m.UserReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	hashedPassword, err := etc.HashPassword(body.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	h.jwtHandler = t.JWTHandler{
		Sub:        body.Username,
		Role:       body.Role,
		SigningKey: h.cfg.SigninKey,
		Log:        h.log,
		Timeout:    480,
	}

	access, refresh, err := h.jwtHandler.GenerateAuthJWT()
	if h.HandleResponse(c, err, http.StatusInternalServerError, InternalServerError, "UserRegister: jwthandler.GenerateAuthJWT()", nil) {
		return
	}

	response, err := h.storage.User().CreateUser(&m.UserReq{
		FullName:        body.FullName,
		Username:        body.Username,
		Password:        hashedPassword,
		Role:            body.Role,
		BirthDate:       body.BirthDate,
		Avatar:          body.Avatar,
		BirthDistrictID: body.BirthDistrictID,
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	c.JSON(http.StatusCreated, m.UserRes{
		ID:              response.ID,
		FullName:        response.FullName,
		Username:        response.Username,
		Role:            response.Role,
		BirthDate:       response.BirthDate,
		BirthDistrictID: response.BirthDistrictID,
		Avatar:          response.Avatar,
		CreatedAt:       response.CreatedAt,
		AccessToken:     access,
		RefreshToken:    refresh,
	})
}

// @Summary 			GET USER
// @Tags 				USER
// @Description 		This method gets user by id
// / @Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param 				id query string true "Id"
// @Success 200 		{object} models.UserRes
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/user/id [get]
func (h *handlerV1) GetUserById(c *gin.Context) {
	id := c.Query("id")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	user, err := h.storage.User().GetUserById(&intId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, user)
}

// @Summary 			GET USER
// @Tags 				USER
// @Description 		This method gets user by Username
// / @Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param 				username query string true "Username"
// @Success 200 		{object} models.UserRes
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/user/username [get]
func (h *handlerV1) GetUserByUsername(c *gin.Context) {
	username := c.Query("username")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if username == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  username")))
		return
	}

	user, err := h.storage.User().GetUserByUsername(&username)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, user)
}

// @Summary 			GET ALL USERS
// @Tags 				USER
// @Description 		this method gets all users
// / @Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Success 200 		{object} models.UsersList
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/user/all [get]
func (h *handlerV1) GetAllUsers(c *gin.Context) {
	page := c.Query("page")
	limit := c.Query("limit")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	users, err := h.storage.User().GetAllUsers(&m.GetAllRequest{
		Page:  intPage,
		Limit: intLimit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, users)
}

// @Summary 			SEARCH USERS
// @Description 		This api search a user
// / @Security    		BearerAuth
// @Tags 				USER
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Param 				name query string true "Name"
// @Success 200 		{object} models.UsersList
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/user/search [get]
func (h *handlerV1) SearchUser(c *gin.Context) {
	page := c.Query("page")
	limit := c.Query("limit")
	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" || name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.User().SearchUser(&m.SearchUserByName{
		Page:  intPage,
		Limit: intLimit,
		Name:  name,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			UPDATE SERVICE
// @Description 		This api updates a service
// /@Security    		BearerAuth
// @Tags 				REPUBLIC
// @Accept 				json
// @Produce 			json
// @Param body 			body models.UpdateUserReq true "Update User"
// @Success 200 		string string
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/user [put]
func (h *handlerV1) UpdateUser(c *gin.Context) {
	var (
		body        m.UpdateUserReq
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }
	// body.OwnerId = ownerId

	hashedPassword, err := etc.HashPassword(body.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	err = h.storage.User().UpdateUser(&m.UpdateUserReq{
		ID:              body.ID,
		FullName:        body.FullName,
		Username:        body.Username,
		Password:        hashedPassword,
		Role:            body.Role,
		BirthDate:       body.BirthDate,
		Avatar:          body.Avatar,
		BirthDistrictID: body.BirthDistrictID,
	})

	if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, errorResponse(errors.New("republic not found")))
		return
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "successfully updated",
	})
}

// @Summary      		DELETE USER
// @Tags         		USER
// @Description  		this method deletes user by id
// / @Security    		BearerAuth
// @Accept 		 		json
// @Produce 		    json
// @Param 				id query string true "user id"
// @Success 200 		string string
// @Failure 400 		string Error models.ErrorResponse
// @Failure 404 		string Error models.ErrorResponse
// @Failure 500		    string Error models.ErrorResponse
// @Router 				/user [delete]
func (h *handlerV1) DeleteUser(c *gin.Context) {
	id := c.Query("id")

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	res, err := h.storage.User().GetUserById(&intId)
	if res == nil {
		c.JSON(http.StatusNotFound, errorResponse(err))
		return
	}

	err = h.storage.User().DeleteUser(&intId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Deleted successfully"})
}
