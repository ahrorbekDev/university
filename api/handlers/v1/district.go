package v1

import (
	"database/sql"
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	m "gitlab.com/university/models"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Summary 			CREATE DISTRICT
// @Description		 	This api create a Sdistrict
// @Tags 				DISTRICT
// /@Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param body 			body models.DistrictReq true "Create District"
// @Success 201 		{object} models.DistrictRes
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/district [post]
func (h *handlerV1) CreateDistrict(c *gin.Context) {
	var (
		body        m.DistrictReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	response, err := h.storage.District().CreateDistrict(m.DistrictReq{
		Title:    strings.ToLower(body.Title),
		Name:     strings.ToLower(body.Name),
		Language: strings.ToLower(body.Language),
		RegionID: body.RegionID,
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// @Summary 			GET ALL DISTRICTS
// @Description 		This method for get all districts
// @Tags 				DISTRICT
// /@Security    		BearerAuth
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Success 200 		{object} models.DistrictsList
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500 		string Error models.ErrorResponse
// @Router 				/district/all [get]
func (h *handlerV1) GetAllDistrict(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.District().GetAllDistricts(&m.GetAllRequest{
		Page:  intPage,
		Limit: intLimit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET DISTRICT
// @Description 		This api gets a district by ID
// /@Security    		BearerAuth
// @Tags 				DISTRICT
// @Accept 				json
// @Produce 			json
// @Param 				id query string true "ID"
// @Success 200 		{object} models.DistrictRes
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/district [get]
func (h *handlerV1) GetDistrictById(c *gin.Context) {

	id := c.Query("id")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  id")))
		return
	}

	response, err := h.storage.District().GetDistrictById(&intId)

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			GET DISTRICT
// @Description 		This api gets a district by Name
// /@Security    		BearerAuth
// @Tags 				DISTRICT
// @Accept 				json
// @Produce 			json
// @Param 				name query string true "Name"
// @Success 200 		{object} models.DistrictRes
// @Failure 404			{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/district/name [get]
func (h *handlerV1) GetDistrictByName(c *gin.Context) {

	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	name = strings.ToLower(name)

	response, err := h.storage.District().GetDistrictByName(&name)

	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			SEARCH DISTRICT
// @Description 		This api search a Districts
// /@Security    		BearerAuth
// @Tags 				DISTRICT
// @Accept 				json
// @Produce 			json
// @Param 				page query int true "Page"
// @Param 				limit query int true "Limit"
// @Param 				name query string true "Name"
// @Success 200 		{object} models.DistrictsList
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/district/search [get]
func (h *handlerV1) SearchDistrict(c *gin.Context) {

	page := c.Query("page")
	limit := c.Query("limit")
	name := c.Query("name")

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }

	if page == "" || limit == "" || name == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid  parameteres")))
		return
	}

	intPage, err := strconv.Atoi(page)
	if err != nil || intPage < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for page parameter")))
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil || intLimit < 1 {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid value for limit parameter")))
		return
	}

	response, err := h.storage.District().SearchDistrict(&m.SearchUserByName{
		Page:  intPage,
		Limit: intLimit,
		Name:  strings.ToLower(name),
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary 			UPDATE DISTRICT
// @Description 		This api updates a district
// /@Security    		BearerAuth
// @Tags 				DISTRICT
// @Accept 				json
// @Produce 			json
// @Param body 			body models.DistrictUpdateData true "Update District"
// @Success 200 		string string
// @Failure 400 		{object} models.ErrorResponse
// @Failure 500 		{object} models.ErrorResponse
// @Router 				/district [put]
func (h *handlerV1) UpdateDistrict(c *gin.Context) {
	var (
		body        m.DistrictUpdateData
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	// ownerId, status := token.GetIdFromToken(c.Request, &h.cfg)
	// if status != 0 {
	// 	c.JSON(http.StatusUnauthorized, errorResponse(errors.New(ErrForbidden.Error())))
	// }
	// body.OwnerId = ownerId

	err = h.storage.District().UpdateDistrict(&m.DistrictUpdateData{
		ID:       body.ID,
		Title:    strings.ToLower(body.Title),
		Name:     strings.ToLower(body.Name),
		Language: strings.ToLower(body.Language),
		RegionID: body.RegionID,
	})

	if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, errorResponse(errors.New("District not found")))
		return
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "successfully updated",
	})
}

// @Summary      		DELETE DISTRICT
// @Tags         		DISTRICT
// @Description  		This method for deleteing district
// /@Security    		BearerAuth
// @Accept 		 		json
// @Produce 		    json
// @Param 				id query string true "id"
// @Success 200 		string string
// @Failure 400 		string Error models.ErrorResponse
// @Failure 500		    string Error models.ErrorResponse
// @Router 				/district [delete]
func (h *handlerV1) DeleteDistrict(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("error : ID parameter is required")))
		return
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("invalid id")))
		return
	}

	err = h.storage.District().DeleteDistrict(&intId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusOK, "Deleted succesfully")
}
