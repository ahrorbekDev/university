package etc

import (
	"regexp"
	"unicode"
)

func IsValidPhoneNumber(phone string) bool {

	normalized := regexp.MustCompile(`\D`).ReplaceAllString(phone, "")

	if len(normalized) != 12 { return false	}

	if normalized[:3] != "998" { return false }

	if 	normalized[4:6] != "33" &&
		normalized[4:6] != "77" &&
		normalized[4:6] != "90" &&
		normalized[4:6] != "91" &&
		normalized[4:6] != "93" &&
		normalized[4:6] != "94" &&
		normalized[4:6] != "97" &&
		normalized[4:6] != "99" {
	return false
}


	for _, r := range normalized[3:] {
		if !unicode.IsDigit(r) {
			return false
		}
	}
	return true
}
