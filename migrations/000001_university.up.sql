CREATE OR REPLACE FUNCTION type_exists(type_name text)
RETURNS boolean AS $$
BEGIN
    RETURN EXISTS(SELECT 1 FROM pg_type WHERE typname = type_name);
END;
$$ LANGUAGE plpgsql;

-- Create the custom ENUM type only if it doesn't exist
DO $$
BEGIN
    IF NOT type_exists('roles') THEN
        CREATE TYPE roles AS ENUM (
            'employee',
            'student'
        );
    END IF;
END $$;


CREATE TABLE IF NOT EXISTS republics (
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(128) UNIQUE NOT NULL,
    data        JSONB NOT NULL
);

CREATE TABLE IF NOT EXISTS regions (
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(128) NOT NULL,
    data        JSONB NOT NULL,
    republic_id INTEGER NOT NULL REFERENCES republics(id)
);

CREATE TABLE IF NOT EXISTS districts (
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(128)  NOT NULL,
    data        JSONB NOT NULL,
    region_id   INTEGER NOT NULL REFERENCES regions(id)
);

CREATE TABLE IF NOT EXISTS users (
    id                  SERIAL PRIMARY KEY,
    username            VARCHAR(128) UNIQUE NOT NULL,
    password            VARCHAR(64) NOT NULL,
    role                roles,
    full_name           VARCHAR(128) NOT NULL,
    birth_date          DATE,
    avatar              VARCHAR(256),
    birth_district_id   INTEGER REFERENCES districts(id),
    refresh_token       VARCHAR(255),
    created_at          TIMESTAMP DEFAULT timezone('Asia/Tashkent', CURRENT_TIMESTAMP) NOT NULL,
    updated_at          TIMESTAMP DEFAULT timezone('Asia/Tashkent', NULL),
    deleted_at          TIMESTAMP DEFAULT timezone('Asia/Tashkent', NULL)
);

CREATE TABLE IF NOT EXISTS departments (
    id      SERIAL PRIMARY KEY,
    name    VARCHAR(128) UNIQUE NOT NULL,
    data    JSONB NOT NULL
);

CREATE TABLE IF NOT EXISTS positions (
    id      SERIAL PRIMARY KEY,
    name    VARCHAR(128) UNIQUE NOT NULL,
    data    JSONB NOT NULL
);

CREATE TABLE IF NOT EXISTS admins(
    id            SERIAL PRIMARY KEY,
    full_name     VARCHAR(255) NOT NULL,
    phone_number  VARCHAR(255) UNIQUE NOT NULL,
    email         VARCHAR(255) UNIQUE NOT NULL,
    password      VARCHAR(255) NOT NULL,
    refresh_token VARCHAR(255),
    role          VARCHAR(255) NOT NULL,
    created_at    TIMESTAMP DEFAULT timezone('Asia/Tashkent', CURRENT_TIMESTAMP) NOT NULL,
    updated_at    TIMESTAMP DEFAULT timezone('Asia/Tashkent', NULL),
    deleted_at    TIMESTAMP DEFAULT timezone('Asia/Tashkent', NULL)
);

